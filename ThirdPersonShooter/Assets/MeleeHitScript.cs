﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeHitScript : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Enemy") || other.CompareTag("Destroyable"))
        {
            other.GetComponent<IDamageable>().OnHit(1);
            StartCoroutine(Player.instance.weapon.Vibration(Player.instance.weapon.vibrationShoot, .1f));

            if (other.transform.GetComponent<IDamageable>().ReturnHP() > 0)
            {
                StartCoroutine(Player.instance.weapon.HitEffect(false));
            }
            else
            {
                StartCoroutine(Player.instance.weapon.HitEffect(true));
            }
        }
    }
}
