﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActivationTrigger : MonoBehaviour {

    public UnityEvent activate;
    public BoxCollider doorCollider;
    private bool alreadyActivated;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !alreadyActivated)
        {
            alreadyActivated = true;
            activate.Invoke();
            Invoke("EnableCollider", 2f);
        }
    }

    void EnableCollider()
    {
        if (doorCollider != null)
        {
            doorCollider.enabled = false;
        }
    }
}
