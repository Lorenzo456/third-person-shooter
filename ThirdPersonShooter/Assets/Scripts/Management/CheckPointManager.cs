﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour
{

    public Checkpoint[] Checkpoints;

    void Awake()
    {
        if (Gamemanager.instance.currentCheckPoint >= 0)
        {
            for (int i = 0; i < Gamemanager.instance.currentCheckPoint + 1; i++)
            {
//                Debug.Log("DISABLE ALL CHECKPOINTS + " + Gamemanager.instance.currentCheckPoint);
                Checkpoints[i].active = false;
                if (i != 0)
                {
                    Checkpoints[i].GetComponent<BoxCollider>().isTrigger = false;
                }
                //Checkpoints[i].DisableSections();
                //Checkpoints[i].gameObject.SetActive(false);
            }
        }
    }

	// Use this for initialization
	void Start () {
        
	    if (Gamemanager.instance.currentCheckPoint == -1)
	    {
	        Player.instance.transform.parent.position = Checkpoints[0].spawnPoint;
            // Player.instance.transform.parent.eulerAngles = new Vector3(0, Checkpoints[0].transform.rotation.y, 0);
	       Camera.main.transform.rotation = Checkpoints[0].transform.rotation;
        }
        else
	    {
            Player.instance.transform.parent.position = Checkpoints[Gamemanager.instance.currentCheckPoint].spawnPoint;
	        Camera.main.transform.rotation = Checkpoints[Gamemanager.instance.currentCheckPoint].transform.rotation; // new Vector3(0, Checkpoints[Gamemanager.instance.currentCheckPoint].transform.rotation.y, 0);
	    }
        Gamemanager.instance.ResetGameManager();
	}
	
}
