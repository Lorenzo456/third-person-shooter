﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Section : MonoBehaviour {
    public DoorScript door1;
    public DoorScript door2;
    public DoorScript door3;
    bool doorIsClosed;
    [HideInInspector]
    public bool triggered;
    public BoxCollider boxCollider;
    public bool dontSkipGameManager = true;

    public GameObject[] enemySection;

    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    void OnEnable()
    {
    }
    void OnDisable()
    {
        Gamemanager.OnCheckEnemyDeath -= OpenDoor;
    }

    void OpenDoor()
    {
        if (door3 != null)
        {
            door3.SwitchDoorState();
        }
        if (door2 != null)
        {
//            Debug.Log("OPEN DOOR 2");
            door2.SwitchDoorState();
            gameObject.SetActive(false);
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player") && !triggered)
        {
            triggered = true;
            if (dontSkipGameManager)
            {
                Gamemanager.instance.SetMusicState(true);
            }
            for (int i = 0; i < enemySection.Length; i++)
            {
                enemySection[i].gameObject.SetActive(true);
                Gamemanager.instance.currentEnemyCount++;
            }
            if(door1 != null && !doorIsClosed)
            {
                CloseDoor();
            }
            Gamemanager.OnCheckEnemyDeath += OpenDoor;
        }

    }

    void CloseDoor()
    {
        doorIsClosed = true;
        door1.SwitchDoorState();
    }
}
