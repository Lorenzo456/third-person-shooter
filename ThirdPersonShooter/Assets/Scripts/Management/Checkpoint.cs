﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public Vector3 spawnPoint;
    public bool active = true;
    public Section[] sections;

    void Start()
    {
     // GameObject debugCube =  Instantiate(GameObject.CreatePrimitive(PrimitiveType.Cube), spawnPoint, Quaternion.identity);
    }
    void OnTriggerEnter( Collider other)
    {
        if (other.CompareTag("Player") && active)
        {
            Gamemanager.instance.currentCheckPoint++;
            active = false;
        }
    }

    public void DisableSections()
    {
        active = false;
        for (int i = 0; i < sections.Length; i++)
        {
            sections[i].triggered = true;
            sections[i].GetComponent<BoxCollider>().isTrigger = false;
        }
    }
}
