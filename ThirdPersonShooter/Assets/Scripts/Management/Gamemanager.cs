﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using AK.Wwise;
public class Gamemanager : MonoBehaviour
{
    public static Gamemanager instance = null;
    private Image deathFade;
    private GameObject deathText;
    private GameObject GrabText;
    private GameObject credits;
    private GameObject paused;
    public int currentEnemyCount;
    public bool enemyDebugStates;

    public delegate void CheckEnemyDeath();
    public static event CheckEnemyDeath OnCheckEnemyDeath;

    public enum GameState { Normal, Attack, AllOutAttack, Menu, WinState};
    public GameState gameState;
    private bool hasCheckedState;
    public int currentCheckPoint = -1;

    private bool menuMusicPlaying = false;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }



    }

    public void SetMusicState(bool combat)
    {
        if (combat)
        {
            AkSoundEngine.SetState("Combat", "InCombat");
        }
        else
        {
            AkSoundEngine.SetState("Combat", "None");
        }
    }

    public void Start()
    {
       // ResetGameManager();
       // gameState = GameState.Normal;
    }

    public void ResetCheckPoints()
    {
        currentCheckPoint = -1;
        SetMusicState(false);
    }
    public void ResetGameManager(bool fromMainMenu = false)
    { 
//        Debug.Log("RESET GAMEMANAGER");
        if (fromMainMenu)
        {
            currentCheckPoint = -1;
        }
        menuMusicPlaying = false;
        currentEnemyCount = 0;
        gameState = GameState.Normal;
        deathFade = GameObject.Find("DeathFade").GetComponent<Image>();
        deathText = GameObject.Find("DeathText");
        GrabText = GameObject.Find("GrabText");
        credits = GameObject.Find("Credits");
        paused = GameObject.Find("Paused");
        paused.SetActive(false);
        credits.SetActive(false);
        deathText.SetActive(false);
        GrabText.SetActive(false);
    }

    public void LoadScene(string Scene)
    {
        SceneManager.LoadScene(Scene, LoadSceneMode.Single);
        if (Scene == "Scene2")
        {
            DontDestroyOnLoad(gameObject);
            gameState = GameState.Normal;
        }
        else
        {
            Destroy(gameObject);
        }
    }


    public void ExitGame()
    {
        Application.Quit();
    }

    private bool fadeIsCheckedAfterWin;
    private bool fadeAfterWin;

    private void DeathFadeWin()
    {
        fadeAfterWin = true;

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            Player.instance.currentHp = 0;
        }
        if (gameState != GameState.Menu)
        {
            if (Player.instance != null && Player.instance.currentHp <= 0 || gameState == GameState.WinState)
            {
//                Debug.Log("FADE WHITE");
                if (gameState != GameState.WinState)
                {
                    deathFade.rectTransform.sizeDelta = new Vector2(Screen.width * 2, Screen.height * 2);
                    deathFade.color = new Color(deathFade.color.r, deathFade.color.g, deathFade.color.b, Mathf.Lerp(deathFade.color.a, 1, Time.deltaTime * .5f));
                }
                else
                {
                    if (!fadeIsCheckedAfterWin)
                    {
                        fadeIsCheckedAfterWin = true;
                        Invoke("DeathFadeWin", 11);
                    }
                    if (fadeAfterWin)
                    {
                        deathFade.rectTransform.sizeDelta = new Vector2(Screen.width * 2, Screen.height * 2);
                        deathFade.color = new Color(deathFade.color.r, deathFade.color.g, deathFade.color.b, Mathf.Lerp(deathFade.color.a, 1, Time.deltaTime * .5f));
                    }
                }

                if (Player.instance.currentHp <= 0)
                {
                    deathText.SetActive(true);
                    if (Input.GetButtonDown("Restart"))
                    {
                        LoadScene("Scene2");
                    }
                }
                else
                {
                    credits.SetActive(true);
                    if (Input.GetButtonDown("Back"))
                    {
                        LoadScene("MenuScreen");
                    }
                }


            }
            else
            {
                if (Input.GetButtonDown("Restart"))
                {
                    if (Time.timeScale == 1)
                    {
                        Time.timeScale = 0;
                        paused.SetActive(true);
                    }
                    else
                    {
                        Time.timeScale = 1;
                        paused.SetActive(false);
                    }
                }
                if (Input.GetButtonDown("Back"))
                {
                    LoadScene("MenuScreen");
                }
            }

            if (gameState != GameState.WinState)
            {



                if (currentEnemyCount > 0)
                {
                    //WWise
                    hasCheckedState = false;
                    if (Player.instance.beingGrabbed)
                    {
                        gameState = GameState.AllOutAttack;
                        AkSoundEngine.SetState("Muziek", "Grabbed");
                        GrabText.SetActive(true);
                    }
                    else
                    {
                        gameState = GameState.Attack;
                        //     AkSoundEngine.SetState("Muziek", "Battle_Kort");
                        GrabText.SetActive(false);
                    }
                }
                else
                {
                    if (!hasCheckedState)
                    {
                        hasCheckedState = true;
                        //Debug.Log("CHECK THIS");
                        if (OnCheckEnemyDeath != null)
                        {
                            OnCheckEnemyDeath();
                        }
                        gameState = GameState.Normal;
                    //    AkSoundEngine.SetState("Muziek", "None");
                        SetMusicState(false);
                    }

                }
            }
            
        }
        else
        {
            if (!menuMusicPlaying)
            {
                Debug.Log("PLAY MENU MUSIC");
                //AkSoundEngine.SetState("Muziek", "Muziek");
                menuMusicPlaying = true;
                SetMusicState(false);
                AkSoundEngine.SetState("Room_Music", "None");
                AkSoundEngine.SetState("Room", "None");
                gameState = GameState.Menu;
            }
        }
    }

    public void WonLevel()
    {
        gameState = GameState.WinState;
        AkSoundEngine.SetState("Room", "Ending_titles");
        Debug.Log("WINSTATE");
    }
}
