﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMachineGun : Weapon {

    BaseEnemy enemy;
    public override void Start()
    {
        base.Start();
        enemy = GetComponentInParent<BaseEnemy>();
    }
    public override void Update()
    {
        
    }

    public override void Shoot(Vector3 point = default(Vector3))
    {
        currentClip--;
        nextFireTime = Time.time + fireRate;
        lineRenderer.SetPosition(0, transform.position);

        RaycastHit hit;
        Vector3 randomPoint = Vector3.zero;//Random.insideUnitSphere * .1f;
        if (Physics.Raycast(transform.position, enemy.transform.parent.forward + randomPoint, out hit, range))
        {
            if (hit.transform.CompareTag("Player"))
            {
                hit.transform.GetComponent<IDamageable>().OnHit(damage);
            }
        }

        if (point == default(Vector3))
        {
            lineRenderer.SetPosition(1, hit.point + randomPoint);
        }

        // Debug.Log("SHOOT");
        StartCoroutine("ShotEffect");
    }

    public override void Reload()
    {
        reloading = true;
        //Wwise reload
        AkSoundEngine.PostEvent("Reload", gameObject);
        StartCoroutine("ReloadTime");
    }

    public IEnumerator ReloadTime()
    {
        yield return new WaitForSeconds(2f);
        currentClip = maxClip;
        reloading = false;
    }

    public override IEnumerator ShotEffect()
    {
        lineRenderer.enabled = true;
        //WWise Gunshots
        AkSoundEngine.PostEvent("Gunshots", gameObject);
        yield return shotLenght;
        lineRenderer.enabled = false;

    }
}
