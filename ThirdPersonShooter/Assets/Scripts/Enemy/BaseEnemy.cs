﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class BaseEnemy : MonoBehaviour
{
    public GameObject player;
    protected FSMSystem fsm;
    public NavMeshAgent agent;
    public Waypoint currentWaypoint;
    public bool checkingForWaypoint;
    public int currentHp;
    public int maxHp = 5;
    public float speed = 5;
    public bool seenPlayer;
    public bool playerInSight;
    public bool canShoot;
    public bool alert;
    public Weapon weapon;

    [HideInInspector] public Animator anim;

    public void SetTransition(Transition t) { fsm.PerformTransition(t); }

    public virtual void Start()
    {
        agent = GetComponentInParent<NavMeshAgent>();
        player = GameObject.Find("Player");
        anim = GetComponent<Animator>();
        currentHp = maxHp;
        agent.speed = speed;
        weapon = GetComponentInChildren<Weapon>();
        MakeFSM();
    }

    public virtual void FixedUpdate()
    {
        fsm.CurrentState.Act(player, gameObject);
        fsm.CurrentState.Reason(player, gameObject);
    }

    public virtual void OnDeath()
    {
        
    }

    public virtual void OnHit(int damageTaken = 0)
    {
        
    }

    public virtual void MakeFSM()
    {

    }

}
