﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : BaseEnemy, IDamageable
{

    public Waypoint[] IdleWaypoints;
    public Waypoint[] AttackWaypoints;
    int currentSpot;
    public bool canReverse;
    public bool goToRandomWaypoint;
    bool reverse;
    public bool hit;
    public float shootingWhileInCoverTimeMin = 3f;
    public float shootingWhileInCoverTimeMax = 8f;

    public override void MakeFSM()
    {
        base.MakeFSM();
        SoldierIdleState idle = new SoldierIdleState();
        idle.AddTransition(Transition.GoToAttack, StateID.SoldierAttackState);

        SoldierAttackState attack = new SoldierAttackState();

        fsm = new FSMSystem();
        fsm.AddState(idle);
        fsm.AddState(attack);
        Debug.Log("SOLDIER INITIALIZED");
    }


    public int ReturnHP()
    {
        return currentHp;
    }

    void Update()
    {
        AnimationHandler();
    }

    public override void OnDeath()
    {
        //base.OnDeath();
        if(currentWaypoint != null)currentWaypoint.taken = false;
        Destroy(gameObject);
    }

    public override void OnHit(int damageTaken = 0)
    {
        hit = true;
        Debug.Log("AHH HIT");
        currentHp -= damageTaken;

        if (currentHp <= 0)
        {
            OnDeath();
        }
        hit = false;
    }


    public void GoToNextWaypoint(Waypoint[] list)
    {
        if (goToRandomWaypoint)
        {
            GoToRandomWaypoint(list);
            return;
        }

        checkingForWaypoint = true;

        //If in Idle state and has no waypoint return Waypoint 0
        if (list == IdleWaypoints && currentWaypoint == null)
        {
            for (int i = 0; i < list.Length; i++)
            {
                if (!list[i].taken)
                {
                    currentWaypoint = IdleWaypoints[i];
                    currentWaypoint.taken = true;
                    agent.SetDestination(currentWaypoint.transform.position);
                    checkingForWaypoint = false;
                    return;
                }
            }
        }

        if (reverse)
        {
            for (int i = list.Length - 1; i > 0; i--)
            {

                if (currentWaypoint == list[i])
                {
                    currentSpot = i;
                }
                else
                {
                    if (currentSpot - 1 <= 0)
                    {
                        reverse = false;
                        GoToNextWaypoint(list);
                        return;
                    }
                    currentSpot--;
                }

                if (!list[currentSpot - 1].taken)
                {
                    currentWaypoint.taken = false;
                    currentWaypoint = list[currentSpot - 1];
                    currentWaypoint.taken = true;
                    agent.SetDestination(currentWaypoint.transform.position);
                    checkingForWaypoint = false;
                    return;
                }
            }

        }
        else
        {
            for (int i = 0; i < list.Length; i++)
            {
                if (currentWaypoint == list[i])
                {
                    if (i + 1 < list.Length && list[i + 1].taken)
                    {
                        continue;
                    }

                    currentWaypoint.taken = false;
                    if (i + 1 == list.Length)
                    {
                        if (canReverse)
                        {
                            reverse = true;
                            GoToNextWaypoint(list);
                            return;
                        }
                        currentWaypoint = list[0];

                    }
                    else
                    {
                        currentWaypoint = list[i + 1];

                    }
                    currentWaypoint.taken = true;
                    agent.SetDestination(currentWaypoint.transform.position);
                    checkingForWaypoint = false;
                    return;
                }

            }
        }



        checkingForWaypoint = false;
    }

    private void GoToRandomWaypoint(Waypoint[] list)
    {
        checkingForWaypoint = true;
        Waypoint tempWaypoint = currentWaypoint;

        while (tempWaypoint == currentWaypoint)
        {
            int tempCurrentSpot = Random.Range(0, list.Length);

            if (!list[tempCurrentSpot].taken)
            {
                if (currentWaypoint != null) currentWaypoint.taken = false;
                currentWaypoint = list[tempCurrentSpot];
                currentWaypoint.taken = true;
                agent.SetDestination(currentWaypoint.transform.position);
            }

        }

        checkingForWaypoint = false;
    }

    public void GoToClosestWaypoint(Waypoint[] list, Vector3 position,bool skipCurrentPoint = false)
    {
        checkingForWaypoint = true;
        Waypoint tempWaypoint = null;
        float prevDistance = 1000;

        for (int i = 0; i < list.Length; i++)
        {
            if (list[i].taken)
            {
                continue;
            }
            if (skipCurrentPoint && currentWaypoint != null)
            {
                if(list[i] == currentWaypoint) continue;
            }

            if (Vector3.Distance(list[i].transform.position, position) < prevDistance)
            {
                prevDistance = Vector3.Distance(list[i].transform.position, position);
                tempWaypoint = list[i];
            }
        }
        if (currentWaypoint != null)
        {
            currentWaypoint.taken = false;
        }

        currentWaypoint = tempWaypoint;
        agent.SetDestination(currentWaypoint.transform.position);
        currentWaypoint.taken = true;
        checkingForWaypoint = false;
    }

    public void AnimationHandler()
    {
        anim.SetFloat("WalkSpeed", agent.speed);
    }
}
