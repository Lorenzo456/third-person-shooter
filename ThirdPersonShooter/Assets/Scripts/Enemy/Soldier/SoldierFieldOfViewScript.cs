﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierFieldOfViewScript : MonoBehaviour {

    public float fieldOfViewAngle;
    public bool playerInSight;
    public Vector3 lastPlayerSighting;
    private Vector3 previousSighting;
    private SphereCollider fieldOfViewCollider;
    private BaseEnemy enemy;
    public void Start()
    {
        fieldOfViewCollider = GetComponent<SphereCollider>();
        enemy = GetComponentInChildren<BaseEnemy>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !enemy.seenPlayer)
        {
            playerInSight = false;
            Vector3 direction = other.transform.position - transform.position;
            float angle = Vector3.Angle(direction, transform.forward);

            if (angle < fieldOfViewAngle * 0.5f)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), direction.normalized, out hit, fieldOfViewCollider.radius))
                {
                    if (other.CompareTag("Player"))
                    {
                        playerInSight = true;
                        enemy.seenPlayer = true;
                        lastPlayerSighting = Player.instance.transform.position;
                    }
                }
            }
        }
    }


    private void Update()
    {
        DebugFieldOfView();
        if (enemy.seenPlayer)
        {
            RaycastHit hit;
            Vector3 direction = Player.instance.transform.position - transform.position;
            if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), direction.normalized, out hit, enemy.weapon.range))
            {
                if (hit.transform.CompareTag("Player"))
                {
                    playerInSight = true;
                    enemy.playerInSight = true;
                }
                else
                {
                    playerInSight = false;
                    enemy.playerInSight = false;
                }
            }
        }

    }

    private void DebugFieldOfView()
    {
        if (!enemy.seenPlayer)
        {
            Vector3 noAngle = Vector3.Normalize(transform.forward);
            Quaternion spreadAngleStart = Quaternion.AngleAxis(-fieldOfViewAngle * 0.5f, new Vector3(0, 1, 0));
            Quaternion spreadAngleEnd = Quaternion.AngleAxis(fieldOfViewAngle * 0.5f, new Vector3(0, 1, 0));

            Vector3 newVector1 = spreadAngleStart * noAngle;
            Vector3 newVector2 = spreadAngleEnd * noAngle;

            Debug.DrawRay(transform.position + new Vector3(0, 1, 0), transform.forward * fieldOfViewCollider.radius, Color.black);
            Debug.DrawRay(transform.position + new Vector3(0, 1, 0), newVector1 * fieldOfViewCollider.radius, Color.black);
            Debug.DrawRay(transform.position + new Vector3(0, 1, 0), newVector2 * fieldOfViewCollider.radius, Color.black);
        }
        else
        {
            Debug.DrawRay(transform.position + new Vector3(0, 1, 0), transform.forward * enemy.weapon.range, Color.red);
        }

    }

}
