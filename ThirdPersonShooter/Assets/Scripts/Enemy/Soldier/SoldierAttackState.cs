﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierAttackState : FSMState {

    public SoldierAttackState()
    {
        stateID = StateID.SoldierAttackState;
    }

    private Soldier soldier;
    private bool firstFrame;
    private bool atDestination;
    private bool shootWhileInCoverCountdown;
    private float shootingWhileInCoverTimer;

    public override void Act(GameObject player, GameObject npc)
    {
        if (!soldier || !firstFrame)
        {
            soldier = npc.GetComponent<Soldier>();
            firstFrame = true;
            Debug.Log("ATTACKSTATE");
            soldier.GoToClosestWaypoint(soldier.AttackWaypoints, Player.instance.transform.position);
            soldier.seenPlayer = true;
        }

        if (Vector3.Angle(soldier.transform.forward, new Vector3(Player.instance.transform.position.x, 0, Player.instance.transform.position.z) - new Vector3(soldier.transform.position.x, 0, soldier.transform.position.z)) > 1)
        {
            Vector3 targetDir = new Vector3(Player.instance.transform.position.x, 0, Player.instance.transform.position.z) - new Vector3(soldier.transform.parent.position.x, 0, soldier.transform.parent.position.z);
            float step = soldier.speed * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(soldier.transform.parent.forward, targetDir, step, 0.0F);
            Debug.DrawRay(soldier.transform.parent.position, newDir, Color.red);
            soldier.transform.parent.rotation = Quaternion.LookRotation(newDir);
        }

        if (soldier.playerInSight && Time.time > soldier.weapon.nextFireTime && !soldier.weapon.reloading && Player.instance.currentHp > 0)
        {
            Debug.Log("SHOOT");
            if (soldier.weapon.currentClip <= 0)
            {
                soldier.weapon.Reload();
            }
            else
            {
                soldier.weapon.Shoot();
            }
        }

        if(soldier.currentWaypoint == null)
            return;

        if (soldier.playerInSight)
        {
            shootWhileInCoverCountdown = false;
            shootingWhileInCoverTimer = Random.Range(soldier.shootingWhileInCoverTimeMin, soldier.shootingWhileInCoverTimeMax) + Time.time;

        }

        if (Vector3.Distance(new Vector3(soldier.transform.position.x, 0, soldier.transform.position.z), new Vector3(soldier.currentWaypoint.transform.position.x, 0, soldier.currentWaypoint.transform.position.z)) < 1 && !atDestination)
        {
            soldier.agent.speed = 0;
            atDestination = true;
            shootingWhileInCoverTimer = Random.Range(soldier.shootingWhileInCoverTimeMin,soldier.shootingWhileInCoverTimeMax) + Time.time;
        }
        else if(atDestination && !soldier.playerInSight && !shootWhileInCoverCountdown)
        {
            shootWhileInCoverCountdown = true;
            shootingWhileInCoverTimer = Random.Range(soldier.shootingWhileInCoverTimeMin, soldier.shootingWhileInCoverTimeMax) + Time.time;
        }
        else if(atDestination && Time.time > shootingWhileInCoverTimer)
        {
           soldier.GoToClosestWaypoint(soldier.AttackWaypoints, Player.instance.transform.position,true);
           atDestination = false;
           soldier.agent.speed = soldier.speed;
        }


    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (!soldier || !firstFrame)
        {
            soldier = npc.GetComponent<Soldier>();
            firstFrame = true;
        }
    }
}
