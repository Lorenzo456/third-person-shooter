﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierIdleState : FSMState {

    public SoldierIdleState()
    {
        stateID = StateID.SoldierIdleState;
    }

    private Soldier soldier;
    private bool firstFrame;
    private bool facingWaypoint;

    public override void Act(GameObject player, GameObject npc)
    {
        if (!soldier || !firstFrame)
        {
            soldier = npc.GetComponent<Soldier>();
            firstFrame = true;
        }

        //if no current waypoint go to next waypoint Or if soldier is at wwaypoint go to next waypoint
        if (soldier.currentWaypoint == null )
        {
            soldier.GoToNextWaypoint(soldier.IdleWaypoints);
            return;
        }
        
        if (Vector3.Distance(new Vector3(soldier.transform.position.x, 0, soldier.transform.position.z), new Vector3(soldier.currentWaypoint.transform.position.x, 0, soldier.currentWaypoint.transform.position.z)) < 2 && !soldier.checkingForWaypoint)
        {
            soldier.GoToNextWaypoint(soldier.IdleWaypoints);
            facingWaypoint = false;
        }
        //Debug.Log(Vector3.Angle(soldier.transform.forward, new Vector3(soldier.currentWaypoint.transform.position.x,0, soldier.currentWaypoint.transform.position.z)  - new Vector3(soldier.transform.position.x,0, soldier.transform.position.z)));

        if (soldier.currentWaypoint.faceWP && !facingWaypoint)
        {
            if (Vector3.Angle(soldier.transform.forward, new Vector3(soldier.currentWaypoint.transform.position.x, 0, soldier.currentWaypoint.transform.position.z) - new Vector3(soldier.transform.position.x, 0, soldier.transform.position.z)) > 1)
            {
                soldier.agent.speed = 0;
                Vector3 targetDir = new Vector3(soldier.currentWaypoint.transform.position.x, 0, soldier.currentWaypoint.transform.position.z) - new Vector3(soldier.transform.parent.position.x, 0, soldier.transform.parent.position.z);
                float step = soldier.speed * Time.deltaTime;
                Vector3 newDir = Vector3.RotateTowards(soldier.transform.parent.forward, targetDir, step, 0.0F);
                Debug.DrawRay(soldier.transform.parent.position, newDir, Color.red);
                soldier.transform.parent.rotation = Quaternion.LookRotation(newDir);
            }
            else
            {
                facingWaypoint = true;
                soldier.agent.speed = soldier.speed;
            }
        }
        

        
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (!soldier || !firstFrame)
        {
            soldier = npc.GetComponent<Soldier>();
            firstFrame = true;
        }

        if (soldier.seenPlayer|| Player.instance.shooting || soldier.hit)
        {
            soldier.SetTransition(Transition.GoToAttack);
        }
    }

    public override void DoBeforeLeaving()
    {
        if (soldier.currentWaypoint != null) soldier.currentWaypoint.taken = false;
        soldier.currentWaypoint = null;
        soldier.agent.speed = 3.5f;
        facingWaypoint = false;
        soldier.agent.SetDestination(soldier.transform.parent.position);
    }
}
