﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEvadeState : FSMState {

    public EnemyEvadeState()
    {
        stateID = StateID.EnemyEvadeState;
    }

    private Enemy enemy;
    private bool firstFrame;
    private bool isRunningToCover;
    private float tempSpeed;
    private bool hasInteracted;
    public override void Act(GameObject player, GameObject npc)
    {
        if (!enemy || !firstFrame)
        {
            enemy = npc.GetComponent<Enemy>();
            firstFrame = true;
            tempSpeed = enemy.speed;
            if (Gamemanager.instance.enemyDebugStates)
                enemy.renderer.material.color = Color.yellow;
        }

        if (!isRunningToCover)
        {
            isRunningToCover = true;
            if (enemy.runToInteractiveObject)
            {
                enemy.RunToInteractiveObject(ref isRunningToCover);
            }
            else
            {
                enemy.RunToNewCover();
            }
        }

        Quaternion targetRotation = Quaternion.LookRotation(enemy.transform.parent.forward, enemy.transform.parent.up);
        enemy.transform.rotation = Quaternion.Slerp(enemy.transform.rotation, targetRotation, enemy.speed * Time.deltaTime);

    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (!enemy || !firstFrame)
        {
            enemy = npc.GetComponent<Enemy>();
            firstFrame = true;
        }

        if (enemy.isRunningToInteractiveObject && enemy.runToInteractiveObject && Vector3.Distance(new Vector3(enemy.transform.parent.position.x, 0, enemy.transform.parent.position.z), new Vector3(enemy.currentWaypoint.transform.position.x, 0, enemy.currentWaypoint.transform.position.z)) < 1 )
        {
            enemy.speed = 0;
            enemy.agent.speed = 0;
            enemy.currentWaypoint.transform.position = enemy.transform.parent.transform.position;
            if (!hasInteracted)
            {
                Debug.Log("HIT INTERACTIVE ELEMENT");
                enemy.anim.SetBool("Interaction", true);
                hasInteracted = true;
                enemy.isRunningToInteractiveObject = false;
            }
            enemy.agent.speed = tempSpeed;
            enemy.speed = tempSpeed;
            return;
        }
        
        if (!enemy.playerInSight && !enemy.playerIsLookingAtMe && Vector3.Distance(new Vector3(enemy.transform.parent.position.x, 0, enemy.transform.parent.position.z), new Vector3(enemy.currentWaypoint.transform.position.x, 0, enemy.currentWaypoint.transform.position.z)) < 3f && !enemy.runToInteractiveObject)
        {
            enemy.speed = 0;
            enemy.agent.speed = 0;
            enemy.currentWaypoint.transform.position = enemy.transform.parent.transform.position;
            enemy.agent.speed = tempSpeed;
            enemy.speed = tempSpeed;
            enemy.SetTransition(Transition.EnemyGoToIdle);
            return;
        }

        if (Vector3.Distance(new Vector3(enemy.transform.parent.position.x, 0, enemy.transform.parent.position.z), new Vector3(enemy.currentWaypoint.transform.position.x, 0, enemy.currentWaypoint.transform.position.z)) < 1 && !enemy.runToInteractiveObject || Player.instance.currentHp <= 0)
        {
            enemy.SetTransition(Transition.EnemyGoToIdle);
        }
        
    }

    public override void DoBeforeLeaving()
    {
        firstFrame = false;
        isRunningToCover = false;
        enemy.anim.SetBool("Interaction", false);
        if (!enemy.gotHit && !enemy.gotHit && !enemy.stunned)
        {
            enemy.agent.speed = tempSpeed;
        }
        else
        {
            enemy.agent.speed = 0;
        }
    }
}
