﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyArm : MonoBehaviour
{
    private BoxCollider boxCollider;
    private Enemy enemy;
    void Start()
    {
        enemy = GetComponentInParent<Enemy>();
        boxCollider = GetComponent<BoxCollider>();
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player") && !enemy.dead)
        {
            if (enemy.DistractBehaviour)
            {
              Debug.Log("THIS IS WHERE WE GRAB");
                enemy.grabAttack = true;
            }
            other.GetComponent<IDamageable>().OnHit(1);
            boxCollider.enabled = false;
        }

        if (other.transform.CompareTag("Interactable") && !enemy.dead)
        {
            other.GetComponent<InteractiveObject>().OnInteraction();
        }
    }
}
