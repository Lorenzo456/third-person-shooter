﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : BaseEnemy, IDamageable
{
    public bool DistractBehaviour;
    public bool hallBehaviour;
    public bool scriptedBehaviour;
    public bool wallGrabBehaviour;
    public bool runToInteractiveObject;
    public bool isRunningToInteractiveObject;
    public float scriptedBehaviourTime;
    public float chanceToRunAfterAttacking = 2;
    public float chanceToAggroAfterHit = 8;
    public Waypoint[] Waypoints;
    public Transform[] HidableObjects;
    public InteractiveObject[] InteractiveObjects;
    [HideInInspector]
    public Vector3 playerDirection;
    public Transform currentHidableObject;
    public float playerLookAtOffset = .6f;
    public bool playerIsLookingAtMe;

    public float randomDecisionTimeMin =5f;
    public float randomDecisionTimeMax = 10f;

    public GameObject bloodSplatter;
    private GameObject blood;
    public float playerDistance;
    [HideInInspector]
    public bool attacking;
    public BoxCollider armCollider;
    [HideInInspector]
    public bool canAttack;
    public bool runAwayAfterAttacking;
    public float attackSpeed = 17f;
    [HideInInspector]
    public bool gotHit;
    [HideInInspector]
    public Renderer renderer;
    [HideInInspector]
    private bool hitSoundPlaying;
    [HideInInspector]
    public bool inPlayerHitZone;
    [HideInInspector]
    public bool stunned;
    [HideInInspector]
    public bool dead;


    public override void Start()
    {
        base.Start();
        armCollider.enabled = false;
        renderer = GetComponentInChildren<Renderer>();
        blood = Instantiate(bloodSplatter, transform.position, Quaternion.identity);
        blood.SetActive(false);
        blood.transform.parent = transform;
    }

    public void SetAreaCost(string nameArea, float cost)
    {
        NavMesh.SetAreaCost(NavMesh.GetAreaFromName(nameArea), cost);
    }

    public float GrabDistance;
    public float GrabRadius = 1;
    [HideInInspector]
    public bool preformingGrab;
    [HideInInspector]
    public bool isGrabbingPlayer;
    [HideInInspector]
    private bool beingPushed;

    public bool canGrabPlayer;
    public bool grabAttack;

    void OnDrawGizmosSelected()
    {
        if (wallGrabBehaviour)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.parent.position + Vector3.down * GrabDistance, GrabRadius);
            Debug.DrawLine(transform.parent.position, transform.parent.position + Vector3.down * GrabDistance);
        }
    }

    public override void MakeFSM()
    {
        base.MakeFSM();
        EnemyIdleState idle = new EnemyIdleState();
        idle.AddTransition(Transition.EnemyGoToEvade, StateID.EnemyEvadeState);
        idle.AddTransition(Transition.EnemyGoAttack, StateID.EnemyAttackState);
        EnemyEvadeState evade = new EnemyEvadeState();
        evade.AddTransition(Transition.EnemyGoToIdle, StateID.EnemyIdleState);
        evade.AddTransition(Transition.EnemyGoAttack, StateID.EnemyAttackState);
        EnemyAttackState attack = new EnemyAttackState();
        attack.AddTransition(Transition.EnemyGoToEvade, StateID.EnemyEvadeState);
        attack.AddTransition(Transition.EnemyGoToIdle,StateID.EnemyIdleState);

        fsm = new FSMSystem();
        fsm.AddState(idle);
        fsm.AddState(evade);
        fsm.AddState(attack);
    }


    public LayerMask avoidanceBoxLayer;

    public override void FixedUpdate()
    {
        if (dead)
            return;
        base.FixedUpdate();

        if (playerDistance < 6)
        {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, 2);

            foreach (Collider col in hitColliders)
            {
                if (col.CompareTag("Avoidance"))
                {
                    inPlayerHitZone = true;
                }
                else
                {
                    inPlayerHitZone = false;
                }
            }
        }

    }

    public void Pushed()
    {
        beingPushed = true;
        transform.parent.GetComponent<Rigidbody>().isKinematic = false;
        transform.parent.GetComponent<Rigidbody>().useGravity = true;
        transform.parent.GetComponent<Rigidbody>().AddForce(Player.instance.transform.forward * 400);
        transform.parent.position = transform.parent.position;
        //GetComponent<BoxCollider>().isTrigger = false;
        isGrabbingPlayer = false;
        grabAttack = false;

        anim.SetBool("Grabbing", false);
        transform.parent.eulerAngles = Vector3.zero;//new Vector3(transform.parent.eulerAngles.x, transform.parent.eulerAngles.y, 0);//Vector3.Lerp(transform.eulerAngles, new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0), Time.deltaTime * 10);
        agent.enabled = false;
        //wwise
        AkSoundEngine.PostEvent("Enemy_Pushed", gameObject);

        // Invoke("ResetAIBehaviourAfterPush", 3f);

    }

    public void ResetAIBehaviourAfterPush()
    {
        beingPushed = false;
        isGrabbingPlayer = false;
        wallGrabBehaviour = false;
        scriptedBehaviour = false;
        //transform.localPosition = new Vector3(0,0,-0.5f);


        agent.enabled = true;
        agent.ResetPath();
        runAwayAfterAttacking = true;
        canGrabPlayer = false;
        Invoke("EnableGrab", 5f);
    }

    void EnableGrab()
    {
        canGrabPlayer = true;
    }

    void EndStun(AnimationEvent animationEvent)
    {
        gotHit = false;
        stunned = false;
        agent.speed = speed;
        anim.SetBool("GotHit", false);
    }

    void Update()
    {
        if (dead)
            return;

        if (gotHit && !stunned)
        {
            stunned = true;
            agent.speed = 0;
            anim.SetBool("GotHit", true);
        }
        if (beingPushed)
        {
            RaycastHit hit1;
            if (Physics.Raycast(transform.parent.position, Vector3.down, out hit1, .5f))
            {
                transform.parent.GetComponent<Rigidbody>().useGravity = false;
                transform.parent.GetComponent<Rigidbody>().isKinematic = true;
                transform.parent.position = hit1.point;//new Vector3(transform.parent.position.x, hit1.point.y, transform.parent.position.z);
                ResetAIBehaviourAfterPush();
            }
        }

        if (isGrabbingPlayer)
        {
            transform.parent.position = Player.instance.grabPosition.transform.position;
            anim.SetBool("Grabbing", true);
            if (Player.instance.currentHp <= 0)
            {
                //      Player.instance.beingGrabbed = false;
                Player.instance.animator.SetTrigger("OnDeath");
                Pushed();
            }

        }
        Debug.DrawRay(transform.parent.position, Vector3.down * .5f, Color.black);

        playerDistance = Vector3.Distance(Player.instance.transform.position, transform.parent.position);

        playerDirection = Player.instance.transform.position - transform.parent.position;
        RaycastHit hit;
        if (Physics.Raycast(transform.parent.position, playerDirection, out hit, 50))
        {
            if (hit.transform.CompareTag("Player"))
            {
               // Debug.Log("Player can see me");
                playerInSight = true;
            }
            else
            {
                playerInSight = false;
            }
        }

        //Use dot product to check if the Player is perpundicular to the forward vector of the enemy
        //Check if player is facing the enemy on a 90 degree angle
        Vector3 forward = transform.TransformDirection(Vector3.forward); 
        if (Vector3.Dot(forward, Player.instance.transform.forward) < 0)
        {
            //Use cross product to check if the player is looking towards the enemy with an offset
            Vector3 checkYCross = Vector3.Cross(forward, Player.instance.transform.forward);
            if (checkYCross.y < playerLookAtOffset && checkYCross.y > -playerLookAtOffset)
            {
                playerIsLookingAtMe = true;
            }
            else
            {
                playerIsLookingAtMe = false;
            }
        }
        else
        {
            playerIsLookingAtMe = false;
        }

  //      Debug.Log(Vector3.Distance(new Vector3(transform.parent.position.x, 0, transform.parent.position.z), new Vector3(currentWaypoint.transform.position.x, 0, currentWaypoint.transform.position.z)));
        
        if(currentWaypoint != null && Vector3.Distance(new Vector3(transform.parent.position.x,0,transform.parent.position.z),new Vector3(currentWaypoint.transform.position.x,0,currentWaypoint.transform.position.z)) < 1 || inPlayerHitZone && fsm.CurrentStateID == StateID.EnemyAttackState  )
        {
            anim.SetFloat("WalkSpeed", 0);

        }
        else
        {
            if (stunned || dead || gotHit || isGrabbingPlayer)
            {
                anim.SetFloat("WalkSpeed", 0);
            }
            else
            {
                anim.SetFloat("WalkSpeed", agent.speed);
            }

        }

        //anim.SetBool("Attacking", canAttack);
    }

    public override void OnHit(int damageTaken = 0)
    {
        gotHit = true;
        attacking = false;
        anim.SetBool("Attacking", false);
        currentHp -= damageTaken;
        //Invoke("EnemyHitSound", .1f);
        if (currentHp <= 0 && !dead)
        {
            OnDeath();
            EnemyDeathSound();
            //Invoke("EnemyDeathSound", .1f);
            //Invoke("OnDeath", .3f);

        }
        else
        {
            if (!blood.activeSelf)
            {
                blood.transform.position = transform.parent.position + new Vector3(0,0.5f,0);
                blood.SetActive(true);
                Invoke("DisableBloodSplatter", .5f);
            }

            Invoke("EnemyHitSound", .1f);
        }

    }

    void DisableBloodSplatter()
    {
        blood.SetActive(false);
    }
    public void EnemyDeathSound()
    {
        AkSoundEngine.PostEvent("Enemy_Dead", gameObject);
    }

    public void EnemyHitSound()
    {
        if(hitSoundPlaying)
            return;
        
        hitSoundPlaying = true;
        AkSoundEngine.PostEvent("Enemy_Hit", gameObject);
        hitSoundPlaying = false;
        //gotHit = false;
    }
    public override void OnDeath()
    {
        agent.speed = 0;
        agent.isStopped = true;
        Gamemanager.instance.currentEnemyCount--;
        dead = true;

        if (currentHidableObject != null && currentHidableObject.GetComponent<Waypoint>() != null)
        {
            currentHidableObject.GetComponent<Waypoint>().taken = false;
        }
        //GetComponent<BoxCollider>().enabled = false;
        gameObject.tag = "Untagged";
        GetComponent<BoxCollider>().isTrigger = false;
        GetComponent<BoxCollider>().center = new Vector3(0,0,0);
        GetComponent<BoxCollider>().size = new Vector3(1, .3f, 1);
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Rigidbody>().isKinematic = false;

        armCollider.enabled = false;
        anim.SetTrigger("Dead");
        //Destroy(transform.parent.gameObject);
    }

    public int ReturnHP()
    {
        return currentHp;
    }

    public void GoToNextWaypoint(Waypoint[] list)
    {

        checkingForWaypoint = true;

        if (currentWaypoint == null)
        {
            for (int i = 0; i < list.Length; i++)
            {
                if (!list[i].taken)
                {
                    currentWaypoint = list[i];
                    currentWaypoint.taken = true;
                    agent.SetDestination(currentWaypoint.transform.position);
                    checkingForWaypoint = false;
                    return;
                }
            }
        }


            for (int i = 0; i < list.Length; i++)
            {
                if (currentWaypoint == list[i])
                {
                    if (i + 1 < list.Length && list[i + 1].taken)
                    {
                        continue;
                    }

                    currentWaypoint.taken = false;

                    if (i + 1 == list.Length)
                    {
                        currentWaypoint = list[0];
                    }
                    else
                    {
                        currentWaypoint = list[i + 1];
                    }

                    currentWaypoint.taken = true;
                    agent.SetDestination(currentWaypoint.transform.position);
                    checkingForWaypoint = false;
                    return;
                }
            
            }



        checkingForWaypoint = false;
    }

    public void GetHidableObject()
    {
        for (int i = 0; i < HidableObjects.Length; i++)
        {
            if (!HidableObjects[i].GetComponent<Waypoint>().taken)
            {
                currentHidableObject = HidableObjects[i];
                currentHidableObject.GetComponent<Waypoint>().taken = true;
                return;
            }
        }
    }

    private bool dodgeTimerStarted;
    private float dodgeTimer;
    private bool canDecideToDoge;
    [HideInInspector]
    public bool runningAway;

    public void Runaway()
    {
        if (runningAway && !DistractBehaviour)
            return;

        if (hallBehaviour)
        {
            currentHidableObject = Player.instance.transform;
            currentWaypoint.transform.position = currentHidableObject.position - Player.instance.transform.forward * 5;
            agent.SetDestination(currentWaypoint.transform.position);
            checkingForWaypoint = false;
            return;
        }

        if (DistractBehaviour)
        {

            if (!dodgeTimerStarted)
            {
                dodgeTimer = Time.time + .5f;
                dodgeTimerStarted = true;
                canDecideToDoge = true;
            }

            if (Time.time > dodgeTimer && canDecideToDoge)
            {
                canDecideToDoge = false;
                //chance to make decision

                float random = UnityEngine.Random.Range(0, 10);
                if (random < 3)
                {
                    currentWaypoint.transform.position = Player.instance.transform.position - new Vector3(0, 0, 5);
                }
                else if (random < 6)
                {
                    currentWaypoint.transform.position = Player.instance.transform.position + new Vector3(5, 0, 0);
                }
                else
                {
                    currentWaypoint.transform.position = Player.instance.transform.position - new Vector3(5, 0, 0);
                }
                agent.SetDestination(currentWaypoint.transform.position);


                dodgeTimer = Time.time + .5f;
                dodgeTimerStarted = false;
            }
            return;
        }

        if (currentHidableObject == null)
        {
            //find an object
            int currentClosest = 0;
            //find closest object to run behind
            for (int i = 0; i < HidableObjects.Length; i++)
            {
                if (Vector3.Distance(transform.position, HidableObjects[0].position) < Vector3.Distance(transform.position, HidableObjects[currentClosest].position))
                {
                    currentClosest = i;
                }
            }
            //set waypoint behind the object - on the opposite side of the player
            currentHidableObject = HidableObjects[currentClosest];

        }

        //find an object to run behind
        if (!hallBehaviour)
        {
            currentWaypoint.transform.position = currentHidableObject.position + Player.instance.transform.forward * 3;
        }

        //go to Waypoint/
        runningAway = true;
        Invoke("SetDestinationAfterDelay", Random.Range(0.3f, 0.6f));
        //agent.SetDestination(currentWaypoint.transform.position);
        anim.SetFloat("WalkSpeed", 0);
    }

    void SetDestinationAfterDelay()
    {
//        Debug.Log("RUNAWAY IS CALLED");
        agent.SetDestination(currentWaypoint.transform.position);
        runningAway = false;

        if (!stunned && !dead && !gotHit && !isGrabbingPlayer)
        {
            anim.SetFloat("WalkSpeed", agent.speed);
        }
    }

    public void RunToNewCover()
    {
        checkingForWaypoint = true;
        if (hallBehaviour)
        {
            currentHidableObject = Player.instance.transform;
            currentWaypoint.transform.position = currentHidableObject.position + Player.instance.transform.forward * 3;
            agent.SetDestination(currentWaypoint.transform.position);
            checkingForWaypoint = false;
            return;
        }
        for (int i = 0; i < HidableObjects.Length; i++)
        {

            if (currentHidableObject == null)
            {
                if (HidableObjects[i].GetComponent<Waypoint>().taken == false)
                {
                    currentHidableObject = HidableObjects[i];
                    currentHidableObject.GetComponent<Waypoint>().taken = true;
                    currentWaypoint.transform.position = currentHidableObject.position + Player.instance.transform.forward * 3;
                    agent.SetDestination(currentWaypoint.transform.position);
                    checkingForWaypoint = false;
                    return;
                }
                continue;
            }

            if (currentHidableObject == null)
            {
                Debug.Log("ALL OBJECTS ARE TAKEN!!");
                return;
            }

            if (currentHidableObject == HidableObjects[i])
            {
                continue;
            }

            if (!HidableObjects[i].GetComponent<Waypoint>().taken)
            {
                currentHidableObject.GetComponent<Waypoint>().taken = false;
                currentHidableObject = HidableObjects[i];
                currentHidableObject.GetComponent<Waypoint>().taken = true;
                currentWaypoint.transform.position = currentHidableObject.position + Player.instance.transform.forward * 3;
                agent.SetDestination(currentWaypoint.transform.position);
                checkingForWaypoint = false;
                return;
            }
        }
        if (currentHidableObject == null)
        {
            Debug.Log("CURRENT OBJECT IS NULL");
        }
        currentWaypoint.transform.position = currentHidableObject.position + Player.instance.transform.forward * 3;
        agent.SetDestination(currentWaypoint.transform.position);
        checkingForWaypoint = false;
    }

    public void RunToInteractiveObject( ref bool isRunningToNewCoverTemp)
    {
        checkingForWaypoint = true;
        for (int i = 0; i < InteractiveObjects.Length; i++)
        {
            if (!InteractiveObjects[i].taken && !InteractiveObjects[i].interacted)
            {
                if (InteractiveObjects[i].onlyIfPlayerIsClose && Vector3.Distance(player.transform.position, InteractiveObjects[i].transform.position) < InteractiveObjects[i].distanceNeeded || !InteractiveObjects[i].onlyIfPlayerIsClose)
                {
                    if (currentHidableObject != null)
                    {
                        currentHidableObject.GetComponent<Waypoint>().taken = false;
                        currentHidableObject = null;
                    }
                    Debug.Log("RUNNING TO INTERACTIVE ELEMENT");
                    currentWaypoint.transform.position = InteractiveObjects[i].targetPosition.transform.position;
                    isRunningToInteractiveObject = true;
                    agent.SetDestination(currentWaypoint.transform.position);
                    checkingForWaypoint = false;
                    return;
                }
            }
        }

        //runToInteractiveObject = false;
        Debug.Log("CAN NOT FIND INTERACTIVE OBJECT");
        isRunningToInteractiveObject = false;
        isRunningToNewCoverTemp = false;
        runToInteractiveObject = false;
        checkingForWaypoint = false;
    }

    public void StartInteraction()
    {
        armCollider.enabled = true;
        attacking = true;
    }

    public void EndInteraction()
    {
        armCollider.enabled = false;
        anim.SetBool("Interaction", false);
        runToInteractiveObject = false;

        attacking = false;
        anim.SetBool("Attacking", false);
        //check if running away or not
        if (Random.Range(0, 10) < chanceToRunAfterAttacking && Gamemanager.instance.gameState != Gamemanager.GameState.AllOutAttack || Gamemanager.instance.gameState != Gamemanager.GameState.AllOutAttack && !grabAttack)
        {
            runAwayAfterAttacking = true;
        }
        else
        {
            runAwayAfterAttacking = false;
        }
    }
    public void StartAttack()
    {
        armCollider.enabled = true;
        attacking = true;   
    }

    public void EndAttack()
    {
        //Debug.Log("ENDATTACK");
        armCollider.enabled = false;
        attacking = false;
        anim.SetBool("Attacking", false);
        //check if running away or not
        if (Random.Range(0, 10) < chanceToRunAfterAttacking && Gamemanager.instance.gameState != Gamemanager.GameState.AllOutAttack || Gamemanager.instance.gameState != Gamemanager.GameState.AllOutAttack && !grabAttack)
        {
            runAwayAfterAttacking = true;
        }
        else
        {
            runAwayAfterAttacking = false;
        }
    }

}
