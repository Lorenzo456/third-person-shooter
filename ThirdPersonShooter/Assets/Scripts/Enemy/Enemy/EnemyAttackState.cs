﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAttackState : FSMState {

    public EnemyAttackState()
    {
        stateID = StateID.EnemyAttackState;
    }

    private Enemy enemy;
    private bool firstFrame;
    private float tempSpeed;

    private float decisionTimer;
    private bool canMakeDecision;
    private bool decisionTimerStarted;
    private bool evade;
    private bool idle;
    private bool resumeAfterStunned;
    private bool goGrabPlayer;

    public void DecisionMaker()
    {
        if (!decisionTimerStarted)
        {
            decisionTimer = Time.time + UnityEngine.Random.Range(enemy.randomDecisionTimeMin, enemy.randomDecisionTimeMax);
            decisionTimerStarted = true;
            canMakeDecision = true;
        }

        if (Time.time > decisionTimer && canMakeDecision)
        {
            canMakeDecision = false;
            //chance to make decision

            float random = UnityEngine.Random.Range(0, 10);
            if (random < 4)
            {
                evade = true;
            }

           // Debug.Log("NEW DECISIONMADE");
            decisionTimer = Time.time + UnityEngine.Random.Range(enemy.randomDecisionTimeMin, enemy.randomDecisionTimeMax);
            decisionTimerStarted = false;
        }
    }

    void GrabEnemyWhenInSight()
    {

        RaycastHit hit;
        if (Physics.SphereCast(enemy.transform.parent.position,enemy.GrabRadius, Vector3.down, out hit, 10))
        {
            enemy.GrabDistance = hit.distance;
            if (hit.transform.CompareTag("Player"))
            {
             //   Debug.Log("CAN GRAB PLAYER");
                goGrabPlayer = true;
               // enemy.anim.SetTrigger("GoGrab");
            }
        }
        else
        {
            enemy.GrabDistance = 5;
        }

    }

    void PreformGrab()
    {
        if (enemy.agent.enabled)
        {
            enemy.agent.enabled = false;
        }

        if (enemy.transform.parent.position != Player.instance.grabPosition.transform.position/*Vector3.Distance(enemy.transform.parent.position, Player.instance.transform.position) > 1 && !enemy.isGrabbingPlayer*/)
        {
            //enemy.transform.parent.position = Vector3.Lerp(enemy.transform.parent.position, Player.instance.transform.position, 10);
            enemy.transform.parent.position = Player.instance.grabPosition.transform.position;
            enemy.transform.parent.rotation = Player.instance.grabPosition.transform.localRotation;
            // Debug.Log("MOVING");
        }
        else
        {
            if (!Player.instance.beingGrabbed)
            {
                AkSoundEngine.PostEvent("Enemy_Grab", enemy.gameObject);
            }
            goGrabPlayer = false;
            if (Player.instance.currentHp > 0)
            {
                enemy.isGrabbingPlayer = true;
                Player.instance.beingGrabbed = true;
            }

            enemy.attacking = false;
            enemy.canAttack = false;
            //wwise
            if (enemy.wallGrabBehaviour)
            {
               // Gamemanager.instance
            }
            //     Debug.Log("TOO CLOSE TO MOVE");

        }
        // Debug.Log("PREFORMING GRAB");
    }

    public override void Act(GameObject player, GameObject npc)
    {
        if (!enemy || !firstFrame)
        {
            enemy = npc.GetComponent<Enemy>();
            firstFrame = true;
            tempSpeed = enemy.agent.speed;
            enemy.agent.speed = enemy.attackSpeed;
            //Wwise 
            AkSoundEngine.PostEvent("Enemy_AttackState", enemy.gameObject);

            if (Gamemanager.instance.enemyDebugStates)
                enemy.renderer.material.color = Color.red;
        }


        if (enemy.wallGrabBehaviour && !enemy.isGrabbingPlayer || enemy.canGrabPlayer && !enemy.isGrabbingPlayer && enemy.grabAttack && !enemy.wallGrabBehaviour)
        {
            if (goGrabPlayer || enemy.grabAttack)
            {
                if (!enemy.wallGrabBehaviour)
                {
                  //  enemy.anim.SetTrigger("GoGrab");
                }

                PreformGrab();
            }
            else
            {
                GrabEnemyWhenInSight();
            }
            return;
        }

        if (!enemy.isGrabbingPlayer || !enemy.grabAttack && !enemy.wallGrabBehaviour)
        {

            if (Gamemanager.instance.gameState != Gamemanager.GameState.AllOutAttack && !enemy.DistractBehaviour && !enemy.runAwayAfterAttacking)
            {
                DecisionMaker();
            }

            enemy.transform.parent.rotation = Quaternion.LookRotation(enemy.playerDirection, enemy.transform.parent.up);

            if (enemy.inPlayerHitZone)
            {
                // Debug.Log("INPLAYER HIT ZONE!!");
                enemy.attacking = false;
                /*
                if (enemy.agent.destination != enemy.transform.parent.position)
                {
                    enemy.agent.SetDestination(enemy.transform.parent.position);
                }*/
                DodgePlayerView();
                return;
            }


            if (enemy.attacking)
            {
                enemy.canAttack = false;
                return;
            }

            if (!enemy.attacking && !enemy.anim.GetBool("Attacking") && Vector3.Distance(new Vector3(enemy.transform.parent.position.x, 0, enemy.transform.parent.position.z), new Vector3(Player.instance.transform.position.x, 0, Player.instance.transform.position.z)) < 2.5f)
            {
              //   Debug.Log("ATTACKK");
                enemy.canAttack = true;
                if (!enemy.runAwayAfterAttacking && !enemy.anim.GetBool("Attacking"))
                {
                    enemy.anim.SetBool("Attacking", true);
                }
                //  enemy.agent.velocity = Vector3.zero;
                 enemy.agent.isStopped = true;
                enemy.agent.SetDestination(enemy.transform.parent.position);
            }
            else if (!enemy.attacking && enemy.currentWaypoint.transform.position != Player.instance.transform.position)
            {
                enemy.agent.isStopped = false;
                enemy.canAttack = false;

                if (enemy.currentWaypoint.transform.position != Player.instance.transform.position)
                {
                    enemy.currentWaypoint.transform.position = Player.instance.transform.position - enemy.transform.forward * 1.1f;
                    enemy.agent.SetDestination(enemy.currentWaypoint.transform.position);
                    // Debug.Log("DESTINATION CHANGE");
                }
                enemy.currentWaypoint.transform.position = Player.instance.transform.position;
                Player.instance.grabbingEnemy = enemy.transform.gameObject;
            }
            else if(enemy.agent.speed == 0)
            {
                Debug.Log("ELESE");
            }
        }
        else
        {
            enemy.transform.parent.position = Player.instance.grabPosition.transform.position;
            Player.instance.grabbingEnemy = enemy.transform.gameObject;
        }


    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (!enemy || !firstFrame)
        {
            enemy = npc.GetComponent<Enemy>();
            firstFrame = true;
        }

        if (evade || enemy.gotHit && UnityEngine.Random.Range(0, 10) < 5 || enemy.gotHit && enemy.wallGrabBehaviour || enemy.runAwayAfterAttacking || Player.instance.currentHp <= 0)
        {
          //  Debug.Log("GOEVADE");
            if (!enemy.hallBehaviour)
            {
                enemy.currentWaypoint.transform.position = enemy.HidableObjects[UnityEngine.Random.Range(0, enemy.HidableObjects.Length)].transform.position;
            }

            if (enemy.DistractBehaviour || Player.instance.currentHp <= 0)
            {
                enemy.SetTransition(Transition.EnemyGoToIdle);
                return;
            }

            if (enemy.runAwayAfterAttacking)
            {
                AkSoundEngine.PostEvent("Enemy_RunAway", enemy.gameObject);
   //             Debug.Log("RUNAWAY FROM ATTACK STATE");
            }
            if (Gamemanager.instance.gameState != Gamemanager.GameState.AllOutAttack || enemy.wallGrabBehaviour && enemy.gotHit)
            {
                enemy.SetTransition(Transition.EnemyGoToEvade);
            }
        }

        if (!enemy.canGrabPlayer && enemy.DistractBehaviour)
        {
            enemy.SetTransition(Transition.EnemyGoToIdle);
        }

       // enemy.gotHit = false;
    }

    private bool dodgeTimerStarted;
    private float dodgeTimer;
    private bool canDecideToDoge;

    void DodgePlayerView()
    {
        if (!dodgeTimerStarted)
        {
            dodgeTimer = Time.time + .5f;
            dodgeTimerStarted = true;
            canDecideToDoge = true;
        }

        if (Time.time > dodgeTimer && canDecideToDoge)
        {
            canDecideToDoge = false;
            //chance to make decision


           // Debug.Log("DODGE");
            if (Random.Range(0, 10) < 4)
            {
                evade = true;
                return;
            }

            float random = UnityEngine.Random.Range(0, 10);
            if (random < 3)
            {
                enemy.currentWaypoint.transform.position = enemy.transform.parent.position - new Vector3(0, 0, 10);
            }
            else if( random < 6)
            {
                enemy.currentWaypoint.transform.position = enemy.transform.parent.position + new Vector3(10, 0, 0);
            }
            else
            {
                enemy.currentWaypoint.transform.position = enemy.transform.parent.position - new Vector3(10, 0, 0);
            }
            enemy.agent.SetDestination(enemy.currentWaypoint.transform.position);
            

            dodgeTimer = Time.time + .5f;
            dodgeTimerStarted = false;
        }
    }

    public override void DoBeforeLeaving()
    {
        enemy.runAwayAfterAttacking = false;
        firstFrame = false;
        enemy.anim.SetBool("Attacking", false);
        //enemy.SetAreaCost("Not Favorable", 6);
        enemy.attacking = false;
        enemy.canAttack = false;
        enemy.agent.isStopped = false;
        if (!enemy.gotHit && !enemy.gotHit && !enemy.stunned)
        {
            enemy.agent.speed = tempSpeed;
        }
        else
        {
            enemy.agent.speed = 0;
        }
        enemy.inPlayerHitZone = false;
        evade = false;

        enemy.isGrabbingPlayer = false;
        enemy.wallGrabBehaviour = false;
        enemy.scriptedBehaviour = false;
    }
}
