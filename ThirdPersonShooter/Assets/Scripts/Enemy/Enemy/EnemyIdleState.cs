﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdleState : FSMState {

    public EnemyIdleState()
    {
        stateID = StateID.EnemyIdleState;
    }

    private Enemy enemy;
    private bool firstFrame;

    private float decisionTimer;
    private bool canMakeDecision;
    private bool decisionTimerStarted;
    private bool evade;
    private bool attack;
    private float scriptTimer = 0;
    private bool scriptedEventPlaying;

    public override void Act(GameObject player, GameObject npc)
    {
        if (!enemy || !firstFrame)
        {
            enemy = npc.GetComponent<Enemy>();
            firstFrame = true;
            if (enemy.currentWaypoint == null)
            {
                enemy.GoToNextWaypoint(enemy.Waypoints);
            }
            if (enemy.currentHidableObject == null)
            {
                enemy.GetHidableObject();
            }

            if (Gamemanager.instance.enemyDebugStates)
            {
                if (enemy.DistractBehaviour)
                {
                   enemy.renderer.material.color = Color.magenta;
                }
                else
               {
                    enemy.renderer.material.color = Color.blue;
                }
            }
            AkSoundEngine.PostEvent("Enemy_Idle", enemy.gameObject);
        }

        Debug.DrawRay(enemy.transform.parent.position, enemy.playerDirection, Color.blue);
        Quaternion targetRotation = Quaternion.LookRotation(enemy.playerDirection, enemy.transform.parent.up); ;
        enemy.transform.parent.rotation = Quaternion.Slerp(enemy.transform.parent.rotation, targetRotation, enemy.speed * Time.deltaTime);

        if (enemy.scriptedBehaviour)
        {
            if (Vector3.Distance(new Vector3(enemy.transform.parent.position.x, 0, enemy.transform.parent.position.z), new Vector3(enemy.currentWaypoint.transform.position.x, 0, enemy.currentWaypoint.transform.position.z)) < 1)
            {
                Idle();
            }
        }
        else
        {
            if (enemy.playerInSight && enemy.playerIsLookingAtMe && !enemy.runningAway || enemy.DistractBehaviour && enemy.playerIsLookingAtMe)
            {
                enemy.Runaway();
            }
            else if (Vector3.Distance(new Vector3(enemy.transform.parent.position.x, enemy.transform.parent.position.y, enemy.transform.parent.position.z), new Vector3(enemy.currentWaypoint.transform.position.x, enemy.currentWaypoint.transform.position.y, enemy.currentWaypoint.transform.position.z)) < 1f && !evade)
            {
                //Debug.Log("SNAP WAYPOINT TO ENEMY");
                enemy.currentWaypoint.transform.position = enemy.transform.parent.transform.position;

            }

            DecisionMaker();
            
        }
        
    }

    public void Idle()
    {
        if (!scriptedEventPlaying)
        {
            scriptTimer = Time.time;
            scriptedEventPlaying = true;
        }
        if (Time.time > scriptTimer + enemy.scriptedBehaviourTime || enemy.gotHit)
        {
            enemy.scriptedBehaviour = false;
            evade = true;
        }
    }
    public void DecisionMaker()
    {
        if (!decisionTimerStarted)
        {
            decisionTimer = Time.time + UnityEngine.Random.Range(enemy.randomDecisionTimeMin, enemy.randomDecisionTimeMax);
            decisionTimerStarted = true;
            canMakeDecision = true;
        }

        if (Time.time > decisionTimer && canMakeDecision)
        {
            canMakeDecision = false;
            //chance to make decision
            if (UnityEngine.Random.Range(0, 10) < 4)
            {
                evade = true;
            }
            else 
            {
                attack = true;

            }
            //Debug.Log("NEW DECISIONMADE");
            decisionTimer = Time.time + UnityEngine.Random.Range(enemy.randomDecisionTimeMin, enemy.randomDecisionTimeMax);
            decisionTimerStarted = false;
        }
    }

    public override void Reason(GameObject player, GameObject npc)
    {
        if (!enemy || !firstFrame)
        {
            //Debug.Log("ENEMY IDLE STATE IS INITIALIZED");
            enemy = npc.GetComponent<Enemy>();
            firstFrame = true;
        }
        if (enemy.wallGrabBehaviour && Player.instance.currentHp > 0 )
        {
            attack = false;
            evade = false;
            enemy.SetTransition(Transition.EnemyGoAttack);
        }

        if (enemy.scriptedBehaviour)
            return;

        if (evade && !enemy.DistractBehaviour && Player.instance.currentHp > 0 && enemy.canGrabPlayer || enemy.gotHit && UnityEngine.Random.Range(0, 10) > 8 && !enemy.DistractBehaviour && Player.instance.currentHp > 0)
        {
            evade = false;
            attack = false;
            enemy.SetTransition(Transition.EnemyGoToEvade);
            return;
        }

        if (attack && enemy.playerDistance < 20 && Player.instance.currentHp > 0 || attack && enemy.playerDistance > 20 && !enemy.playerIsLookingAtMe && Player.instance.currentHp > 0 || enemy.gotHit && UnityEngine.Random.Range(0,10) < enemy.chanceToAggroAfterHit && Player.instance.currentHp > 0 || Gamemanager.instance.gameState == Gamemanager.GameState.AllOutAttack && !enemy.DistractBehaviour && Player.instance.currentHp > 0)
        {
            attack = false;
            evade = false;
            enemy.SetTransition(Transition.EnemyGoAttack);
            return;
        }

        evade = false;
        attack = false;
    }

    public override void DoBeforeLeaving()
    {
        //Debug.Log("OMG ITS ACTUALLY DDONE");
        firstFrame = false;
        evade = false;
        attack = false;
    }
}
