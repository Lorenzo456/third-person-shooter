﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour {

    public new Light light;
    public float minIntensity;
    public float maxIntensity;
    public float chanceToStopFlicker = 7;
    private bool flickTime;
    private bool canFlicker = true;

    void Start()
    {
        light = GetComponent<Light>();
        light.intensity = 0;
    }

    void Update()
    {
        if (light.intensity >= minIntensity && light.intensity <= maxIntensity)
        {
            light.intensity = Mathf.Lerp(light.intensity, light.intensity + 1, Time.deltaTime * 90);
        }
        else
        {
            if (flickTime == false && canFlicker)
            {
                StartCoroutine("Flicker");
            }
        }

    }

    IEnumerator Flicker()
    {
        flickTime = true;
        yield return new WaitForSeconds(Random.Range(0, .02f));
        if (Random.Range(0, 10) > chanceToStopFlicker)
        {
            StartCoroutine("StopFlickering");
            yield return null;
        }

        AkSoundEngine.PostEvent("Light_Bulp_Flicker", gameObject);
        light.intensity = minIntensity;
        flickTime = false;
    }

    IEnumerator StopFlickering()
    {
        canFlicker = false;
        yield return new WaitForSeconds(Random.Range(5, 8));
        canFlicker = true;
    }
}
