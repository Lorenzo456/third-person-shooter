﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Destroyable : MonoBehaviour, IDamageable {

    public int hp = 5;
    public bool endObjective;

    public void OnHit(int damageTaken = 0)
    {
        hp -= damageTaken;

        if (hp <= 0)
        {
            if (endObjective)
            {
                Gamemanager.instance.WonLevel();
            }
            Invoke("OnDeath", .1f);
            //OnDeath();
        }
    }

    public void OnDeath()
    {
        //gameObject.SetActive(false);
        Destroy(gameObject);
    }

    public int ReturnHP()
    {
        return hp;
    }

}
