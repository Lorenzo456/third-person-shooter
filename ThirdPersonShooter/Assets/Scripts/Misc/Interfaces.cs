﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    void OnHit(int damageTaken = 0);

    void OnDeath();

    int ReturnHP();

}
