﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventHandler : MonoBehaviour {

    public void StartMelee(AnimationEvent animationEvent)
    {
        Player.instance.meleeAttackCollider.enabled = true;
        Player.instance.meleeAttack = true;
    }

    public void EndMelee(AnimationEvent animationEvent)
    {
        Player.instance.meleeAttack = false;
    }

    public void EndMeleeCollider(AnimationEvent animationEvent)
    {
        Player.instance.meleeAttackCollider.enabled = false;
    }

    public void ReloadSoundEffect(AnimationEvent animationEvent)
    {
        //Wwise reload
        AkSoundEngine.PostEvent("Reload", gameObject);
    }

    public void ReloadgunEnd(AnimationEvent animationEvent)
    {
        Player.instance.weapon.Reloaded();
    }

    public void StartShooting(AnimationEvent animationEvent)
    {
        //Player.instance.shooting = true;
        Player.instance.weapon.Shoot(Player.instance.weapon.hit.point);
        /*
        Player.instance.weapon.Shoot(Player.instance.weapon.hit2.point);
        Player.instance.weapon.Shoot(Player.instance.weapon.hit3.point);
        Player.instance.weapon.Shoot(Player.instance.weapon.hit4.point);
        Player.instance.weapon.Shoot(Player.instance.weapon.hit5.point);
        */
        Player.instance.shooting = false;
    }

    public void EndShooting(AnimationEvent animationEvent)
    {
        //Debug.Log("ENDSHOT");
        Player.instance.shooting = false;
        Player.instance.animator.SetBool("Shooting", false);
    }

}
