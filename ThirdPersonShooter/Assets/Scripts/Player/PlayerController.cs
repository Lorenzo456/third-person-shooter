﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class PlayerController : MonoBehaviour, IDamageable {

    public float speed = 2f;
    private float originalSpeed;
    public float runningSpeed = 8f; 
    public float currentSpeed;
    public float gravity = 15F;

    public float verticalVelocity;

    float moveFB;
    float moveLR;
    private Camera cam;

    private bool falling;
    private float tempMovement;
    private PlayerHitZone playerHitZone;
    CharacterController player;
    public int mashAmount;
    public float mashCounter;
    public float mashTime = 1f;
    public int mashMax = 10;
    private bool dead;

    [Header("Controller Settings")]
    protected GamePadState state;

    void Start () {
        player = GetComponent<CharacterController>();
        playerHitZone = GetComponentInChildren<PlayerHitZone>();
        cam = Camera.main;
        //AkSoundEngine.PostEvent("Movement", gameObject);
        originalSpeed = speed;
        dead = false;
    }

    private void Update()
    {

        if (Player.instance.currentHp > 0)
        {
            Movement();
            if (Input.GetAxis("Aim") > 0)
            {
                Player.instance.aiming = true;
                playerHitZone.collider.enabled = true;
                Player.instance.reticle.enabled = true;
            }
            else
            {
                Player.instance.aiming = false;
                playerHitZone.collider.enabled = false;
                Player.instance.reticle.enabled = false;
            }
            if (Input.GetAxis("Shoot") > 0)
            {
                Player.instance.pullingTrigger = true;
            }
            else
            {
                Player.instance.pullingTrigger = false;
            }

            if (Input.GetButton("Run"))
            {
                Player.instance.running = true;
            }
            else
            {
                Player.instance.running = false;
            }

            if (Input.GetButton("Reload"))
            {
                Player.instance.reloading = true;
            }
            else
            {
                Player.instance.reloading = false;
            }

            if (Input.GetButtonDown("Melee"))
            {
                Player.instance.animator.SetTrigger("MeleeAttack");
            }
            else
            {
                Player.instance.animator.ResetTrigger("MeleeAttack");
            }

            if (Player.instance.beingGrabbed)
            {
                DisableAnimations();
                HandlePlayerMashing();
            }
            else
            {
                mashAmount = 0;
            }
        }
        else
        {
            DisableAnimations();
        }

        AnimationHandler();
        //Debug.Log("Mashamount: " + mashAmount);
    }

    public void HandlePlayerMashing()
    {
        if (Time.time > mashCounter)
        {
            mashAmount = 0;
        }
        
        if (Input.GetButtonDown("Mash"))
        {
            mashCounter = Time.time + mashTime;
            mashAmount++;
            if (mashAmount >= mashMax)
            {
                Player.instance.beingGrabbed = false;

                Player.instance.grabbingEnemy.GetComponent<Enemy>().Pushed();
                speed = originalSpeed;
            }
        }
    }

    public void DisableAnimations()
    {
        Player.instance.running = false;
        Player.instance.pullingTrigger = false;
        Player.instance.aiming = false;
        Player.instance.reloading = false;
        Player.instance.weapon.preShoot = false;
        Player.instance.shooting = false;
        speed = 0;
        tempMovement = 0;
    }

    public void AnimationHandler()
    {
        Player.instance.animator.SetBool("PreShoot", Player.instance.weapon.preShoot);
        Player.instance.animator.SetBool("Shooting", Player.instance.shooting);
        Player.instance.animator.SetBool("Aiming", Player.instance.aiming);
        Player.instance.animator.SetFloat("Movement", tempMovement);
        Player.instance.animator.SetBool("Grabbed", Player.instance.beingGrabbed);
        Player.instance.animator.SetBool("AimingUp", Player.instance.aimingUp);
        Player.instance.animator.SetBool("AimingDown", Player.instance.aimingDown);
        Player.instance.animator.SetFloat("CurrentHp", Player.instance.currentHp);
    }

    private bool moveSoundIsOn;
    void Movement() {

        moveFB = Input.GetAxis("Vertical") * speed;
        moveLR = Input.GetAxis("Horizontal") * speed;

        float tempmoveFB = Input.GetAxis("Vertical");
        float tempmoveLR = Input.GetAxis("Horizontal");

        if (tempmoveFB < 0)
        {
            tempmoveFB *= -1;
        }

        if (tempmoveLR < 0)
        {
            tempmoveLR *= -1;
        }
        tempMovement = tempmoveLR + tempmoveFB;
      //  AkSoundEngine.SetRTPCValue("Movement", tempMovement * 100);

        Vector3 movement = new Vector3(moveLR, verticalVelocity, moveFB);
        movement = Vector3.Normalize(transform.rotation * movement);



        if (Player.instance.running) 
        {
            currentSpeed = runningSpeed;
        }
        else
        {
            currentSpeed = speed;
        }


        if (!IsGrounded())
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }
        else
        {
            verticalVelocity = -gravity;
        }

        transform.rotation = Quaternion.Euler(0,cam.transform.eulerAngles.y,0);

        if (Player.instance.aiming)
        {
            currentSpeed *= .5f;
        }

        player.Move(movement * currentSpeed * Time.deltaTime);

        if (tempMovement > 0 && !Player.instance.beingGrabbed && Player.instance.currentHp > 0 && !moveSoundIsOn)
        {
            moveSoundIsOn = true;
            //wwise
            AkSoundEngine.PostEvent("Player_Movement", gameObject);
        }
        else
        {
            moveSoundIsOn = false;
            AkSoundEngine.PostEvent("Stop_Player_Movement", gameObject);
        }
    }

    bool IsGrounded()
    {
        if (player.isGrounded)
        {
            falling = false;
            return true;
        }
        Vector3 bottom = player.transform.position - new Vector3(0, player.height / 2, 0);
        RaycastHit hit;

        if (!Physics.Raycast(bottom, new Vector3(0, -1, 0), out hit, 0.7f))
        {
            falling = true;
        }

        if(Physics.Raycast(bottom, new Vector3(0,-1,0), out hit, 0.5f) && !falling)
        {
            player.Move(new Vector3(0, -hit.distance, 0));
            return true;
        }

        return false;
    }

    public void OnDeath()
    {
        dead = true;
        Debug.Log("YOU LOSE");
        Player.instance.animator.SetTrigger("OnDeath");
    }

    public void OnHit(int damageTaken = 0)
    {
        StartCoroutine(Vibration(5, .1f));
        Player.instance.currentHp -= damageTaken;

        //Player.instance.currentHpText.text = Player.instance.currentHp.ToString();
        double temp = 100 * ((double)Player.instance.currentHp / (double)Player.instance.maxHp);
      //  Debug.Log(temp);
        if (temp < 50)
        {
            Player.instance.hpPrecentageText.color = Color.red;
        }

        if (temp < 0)
        {
            Player.instance.hpPrecentageText.text = (0).ToString() + "%";
        }
        else
        {
            Player.instance.hpPrecentageText.text = ((int)temp).ToString() + "%";
        }

        //wwise
        AkSoundEngine.PostEvent("Player_Hit", gameObject);

        if (Player.instance.currentHp <= 0 && !dead)
        {
            Player.instance.hpPrecentageText.text = 0.ToString() + "%";
            OnDeath();
        }
        else
        {
            Player.instance.animator.SetTrigger("OnHit");
        }
    }

    public int ReturnHP()
    {
        return Player.instance.currentHp;
    }

    public virtual IEnumerator Vibration(float vibration = 0, float vibrationTime = .1f)
    {
        state = GamePad.GetState(0);
        if (state.IsConnected && Player.instance.currentHp >0)
        {
            GamePad.SetVibration(0, vibration, vibration);
            yield return new WaitForSeconds(vibrationTime);
            GamePad.SetVibration(0, 0, 0);
        }
    }
}
