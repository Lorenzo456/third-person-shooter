﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public static Player instance = null;
    public int maxHp;
    public int currentHp;
    public Text maxHpUI;
    public Text currentHpText;
    public Text hpPrecentageText;
    public bool pullingTrigger;
    public bool aiming;
    public bool running;
    public bool reloading;
    public bool shooting;
    public bool aimingUp;
    public bool aimingDown;
    public bool meleeAttack;
    public Weapon weapon;


    public Image reticle;
    public GameObject hitUI;
    public GameObject hitKillUI;
    public GameObject grabPosition;
    public bool beingGrabbed;
    public GameObject grabbingEnemy;
    public BoxCollider meleeAttackCollider;

    public Animator animator;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

    }

    private void Start()
    {
        maxHpUI = GameObject.Find("MaxHP").GetComponent<Text>();
        //currentHpText = GameObject.Find("CurrentHP").GetComponent<Text>();
        hpPrecentageText = GameObject.Find("HPPrecentage").GetComponent<Text>();
        //currentHpText.text = maxHp.ToString();
        //maxHpUI.text = maxHp.ToString();
        hpPrecentageText.text = 100.ToString() + "%";
        reticle = GameObject.Find("Reticle").GetComponent<Image>();
        hitUI = GameObject.Find("HitUI");
        hitUI.SetActive(false);
        hitKillUI = GameObject.Find("HitKillUI");
        hitKillUI.SetActive(false);
        animator = GetComponent<Animator>();
        currentHp = maxHp;
        weapon = GetComponentInChildren<Weapon>();
    }

  
}
