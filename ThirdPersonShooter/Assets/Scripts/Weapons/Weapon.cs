﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;

public class Weapon : MonoBehaviour
{
    public float fireRate = .25f;
    public float range = 100;
    public int damage = 1;
    public Camera playerCamera;
    public LineRenderer lineRenderer;
    public LayerMask ignoreMask;

    public AudioSource audioSource;
    public WaitForSeconds shotLenght = new WaitForSeconds(.02f);
    public WaitForSeconds hitLenght = new WaitForSeconds(.2f);
    public float nextFireTime;
    private Light muzzleFlash;

    public int maxBullets;
    int bullets;
    public int maxClip;
    public int currentClip;
    public bool reloading;

    public Text currentBulletsUI;
    public Text clipUI;

    public bool triggerRelease;
    public bool released;
    public bool shooting;
    public bool preShoot;

    [Header("Controller Settings")]
    protected GamePadState state;
    public float vibrationShoot = 1f;
    public float vibrationReload;

    //wwise
    private float emptyShotTimer;
    public float emptyShotTimerRate = .5f;
    //camera
    public float offset = 2;

    public virtual void Start()
    {

        currentBulletsUI = GameObject.Find("CurrentBulletsUI").GetComponent<Text>();
        clipUI = GameObject.Find("MaxClipUI").GetComponent<Text>();
        audioSource = GetComponent<AudioSource>();
        playerCamera = Camera.main;
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        bullets = int.MaxValue;
        muzzleFlash = GetComponent<Light>();
        muzzleFlash.enabled = false;
    }

    public void OnShootBehaviour(RaycastHit hit)
    {
        //OnShoot
        if (Player.instance.pullingTrigger && Time.time > nextFireTime && !reloading)
        {

            if (currentClip <= 0)
            {
                if (released && Time.time > emptyShotTimer && !shooting)
                {
                    emptyShotTimer = Time.time + emptyShotTimerRate;
                    released = false;
                    //WWise Gun empty
                    AkSoundEngine.PostEvent("Gun_Empty", gameObject);
     //               Debug.Log("NO BULLETS");
                }
                return;
            }


            if (triggerRelease && !released || preShoot)
                return;


            //Shoot(hit.point);
            preShoot = true;
        }
    }

    public RaycastHit hit;
    public RaycastHit hit2;
    public RaycastHit hit3;
    public RaycastHit hit4;
    public RaycastHit hit5;

    public virtual void Update()
    {
        if (!Player.instance.pullingTrigger && !shooting)
        {
            released = true;
        }

        if (currentBulletsUI && clipUI)
        {
            //currentBulletsUI.text = "∞";//bullets.ToString();
            if (currentClip < maxClip / 2)
            {
                clipUI.color = Color.red;
            }
            else
            {
                clipUI.color = Color.white;
            }
            clipUI.text = currentClip.ToString();
        }


        Vector3 rayOrigin = playerCamera.ViewportToWorldPoint(new Vector3(.5f, .5f, 0));

        Debug.DrawRay(rayOrigin, playerCamera.transform.forward * range, Color.green);

        Vector3 rotationYPlus = Quaternion.Euler(0, offset, 0) * playerCamera.transform.forward;
        Vector3 rotationYMin = Quaternion.Euler(0, -offset, 0) * playerCamera.transform.forward;

        Vector3 rotationXPlus = Quaternion.AngleAxis(offset, playerCamera.transform.right) * playerCamera.transform.forward;
        Vector3 rotationXMin = Quaternion.AngleAxis(-offset, playerCamera.transform.right) * playerCamera.transform.forward;
        /*
        Debug.DrawRay(rayOrigin, rotationXPlus * range, Color.red);
        Debug.DrawRay(rayOrigin, rotationXMin * range, Color.red);
        Debug.DrawRay(rayOrigin, rotationYPlus * range, Color.blue);
        Debug.DrawRay(rayOrigin, rotationYMin * range, Color.blue);
        */
        if (Physics.Raycast(rayOrigin, playerCamera.transform.forward, out hit, range, ignoreMask))
        {
            OnRayCastHover(hit);

            OnShootBehaviour(hit);

        }
         if(Physics.Raycast(rayOrigin, rotationYPlus, out hit2, range, ignoreMask))
        {
            OnShootBehaviour(hit2);
        }
         if (Physics.Raycast(rayOrigin, rotationYMin, out hit3, range, ignoreMask))
        {
            OnShootBehaviour(hit3);
        }
         if (Physics.Raycast(rayOrigin, rotationXPlus, out hit4, range, ignoreMask))
        {
            OnShootBehaviour(hit4);
        }
         if (Physics.Raycast(rayOrigin, rotationXMin, out hit5, range, ignoreMask))
        {
            OnShootBehaviour(hit5);
        }

        /*
       else
       {
           Player.instance.reticle.color = Color.black;

           if (Player.instance.pullingTrigger && Time.time > nextFireTime && !reloading)
           {
               //WWise Gun empty
               if (currentClip <= 0)
               {
                   if (released && Time.time > emptyShotTimer && !shooting)
                   {
                       emptyShotTimer = Time.time + emptyShotTimerRate;
                       released = false;
                       //WWise Gun empty
                       AkSoundEngine.PostEvent("Gun_Empty", gameObject);
                       Debug.Log("NO BULLETS");
                   }
                   return;
               }

               if (triggerRelease && !released)
                   return;

               // Shoot();
               preShoot = true;
           }

       }
       */


        if (Player.instance.reloading)
        {
            Reload();
        }

    }

    public virtual void OnRayCastHover(RaycastHit hit)
    {
        //ON Hover
        if (hit.transform.CompareTag("Destroyable") || hit.transform.CompareTag("Enemy"))
        {
            //   Debug.Log("INTERACTABLE");
            Player.instance.reticle.color = Color.red;

        }
        else
        {
            Player.instance.reticle.color = Color.white;
        }
    }

    public virtual void Shoot(Vector3 point = default(Vector3))
    {
        released = false;
        Player.instance.shooting = true;
        nextFireTime = Time.time + fireRate;
        currentClip--;

        clipUI.text = currentClip.ToString();
        offset = UnityEngine.Random.Range(2.5f, 5f);
        if (hit.transform.CompareTag("Destroyable") || hit.transform.CompareTag("Enemy"))
        {
            hit.transform.GetComponent<IDamageable>().OnHit(damage);
            if (hit.transform.GetComponent<IDamageable>().ReturnHP() > 0)
            {
                StartCoroutine(HitEffect(false));
            }
            else
            {
                StartCoroutine(HitEffect(true));
            }
        }

        if (hit2.transform.CompareTag("Destroyable") || hit2.transform.CompareTag("Enemy"))
        {
            hit2.transform.GetComponent<IDamageable>().OnHit(damage);
            if (hit2.transform.GetComponent<IDamageable>().ReturnHP() > 0)
            {
                StartCoroutine(HitEffect(false));
            }
            else
            {
                StartCoroutine(HitEffect(true));
            }
        }

        if (hit3.transform.CompareTag("Destroyable") || hit3.transform.CompareTag("Enemy"))
        {
            hit3.transform.GetComponent<IDamageable>().OnHit(damage);
            if (hit3.transform.GetComponent<IDamageable>().ReturnHP() > 0)
            {
                StartCoroutine(HitEffect(false));
            }
            else
            {
                StartCoroutine(HitEffect(true));
            }
        }

        if (hit4.transform.CompareTag("Destroyable") || hit4.transform.CompareTag("Enemy"))
        {
            hit4.transform.GetComponent<IDamageable>().OnHit(damage);
            if (hit4.transform.GetComponent<IDamageable>().ReturnHP() > 0)
            {
                StartCoroutine(HitEffect(false));
            }
            else
            {
                StartCoroutine(HitEffect(true));
            }
        }

        if (hit5.transform.CompareTag("Destroyable") || hit5.transform.CompareTag("Enemy"))
        {
            hit5.transform.GetComponent<IDamageable>().OnHit(damage);
            if (hit5.transform.GetComponent<IDamageable>().ReturnHP() > 0)
            {
                StartCoroutine(HitEffect(false));
            }
            else
            {
                StartCoroutine(HitEffect(true));
            }
        }

        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1,hit.point);

        lineRenderer.SetPosition(2,transform.position);
        lineRenderer.SetPosition(3,hit2.point);

        lineRenderer.SetPosition(4,transform.position);
        lineRenderer.SetPosition(5, hit3.point);

        lineRenderer.SetPosition(6,transform.position);
        lineRenderer.SetPosition(7,hit4.point);

        lineRenderer.SetPosition(8,transform.position);
        lineRenderer.SetPosition(9,hit5.point);
        

        StartCoroutine("ShotEffect");
        StartCoroutine(Vibration(vibrationShoot, .1f));
        preShoot = false;

    }

    public virtual void Reload()
    {
        if (!reloading && currentClip < maxClip && bullets > 0)
        {
            reloading = true;
            Player.instance.animator.SetBool("Reloading", true);


            //("Reloaded",2f);
        }
    }

    public virtual void Reloaded()
    {
        nextFireTime = Time.time + (fireRate * 2);

        reloading = false;

        if (bullets > maxClip - currentClip)
        {
            bullets = bullets - (maxClip - currentClip);
            currentClip = maxClip;
        }
        else
        {
            currentClip = currentClip + bullets;
            bullets = 0;
        }
        Player.instance.animator.SetBool("Reloading", false);
    }

    public virtual IEnumerator Vibration(float vibration = 0, float vibrationTime = .1f)
    {
        state = GamePad.GetState(0);
        if (state.IsConnected)
        {
            GamePad.SetVibration(0, vibration, vibration);
            yield return new WaitForSeconds(vibrationTime);
            GamePad.SetVibration(0, 0, 0);
        }
    }

    public virtual IEnumerator ShotEffect()
    {
        lineRenderer.enabled = true;
        muzzleFlash.enabled = true;
        //audioSource.Play();
        
        //WWise Gunshots
        AkSoundEngine.PostEvent("Shotgun", gameObject);
        yield return shotLenght;
        lineRenderer.enabled = false;
        muzzleFlash.enabled = false;
        released = false;
    }

    public virtual IEnumerator HitEffect(bool killed)
    {
        if (killed)
        {
            Player.instance.hitKillUI.SetActive(true);
        }
        else
        {
            Player.instance.hitUI.SetActive(true);
        }
        yield return hitLenght;
        if (killed)
        {
            Player.instance.hitKillUI.SetActive(false);
        }
        else
        {
            Player.instance.hitUI.SetActive(false);
        }
    }

}
