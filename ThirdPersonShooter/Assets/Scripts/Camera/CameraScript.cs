﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{

    public Transform lookAt;
    public Transform lookAtRight;
    public Transform cameraPivot;
    private Transform camTransform;
    private Transform camMask;

    public float minDistance = 1f;
    public float maxDistance = 5f;
    public float smooth = 10f;
    public bool aiming;
    public bool shooting;

    private float currentDistance;
    private float originalDistance;
    private float distance;

    private float currentX;
    private float currentY;
    public float sensitivityX = 8;
    public float sensitivityY = 4;
    private float sensitivitySpeed = 1;
    public float Y_ANGLE_MIN = -10;
    public float Y_ANGLE_MAX = 70;

    void Start()
    {
        camTransform = transform;
        camMask = camTransform;
        lookAt.position = cameraPivot.position;
        distance = maxDistance;
        currentDistance = distance;
        originalDistance = distance;
        lookAt = GameObject.Find("CameraLook").transform;
        lookAtRight = GameObject.Find("RightShoulder").transform;
        cameraPivot = GameObject.Find("CameraPivot").transform;
    }

    void Update()
    {
        if (Player.instance.aiming)
        {
            //lookAt.position = lookAtRight.position;
            lookAt.position = Vector3.Lerp(lookAt.position, lookAtRight.position, Time.deltaTime * 2);
            // currentDistance = distance / 2;
            aiming = true;
            currentDistance = Mathf.Lerp(currentDistance,Mathf.Clamp(distance, minDistance, originalDistance / 2), Time.deltaTime * smooth);
            sensitivitySpeed = .35f;
        }
        else
        {
            aiming = false;
            //lookAt.position = cameraPivot.position;
            lookAt.position = Vector3.Lerp(lookAt.position, cameraPivot.position, Time.deltaTime * 2);
            currentDistance = Mathf.Lerp(currentDistance, distance, Time.deltaTime * (smooth /2));
            sensitivitySpeed = 1;
            //currentDistance = Mathf.Lerp(distance, maxDistance, Time.deltaTime * smooth);
        }

        if (Player.instance.pullingTrigger)
        {
            shooting = true;
        }
        else
        {
            shooting = false;
        }


        currentX += Input.GetAxis("Mouse X") * sensitivityX * sensitivitySpeed;
        currentY += Input.GetAxis("Mouse Y") * sensitivityY * sensitivitySpeed;
        currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
        occludeRay(lookAt.position);

    }

    void LateUpdate()
    {
        CameraMovement();

    }

    void CameraMovement()
    {
        Vector3 dir = new Vector3(0, 0, -currentDistance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX , 0);
        //camTransform.position = lookAt.position + rotation * dir;
        if (aiming)
        {
            camTransform.position = lookAt.position + rotation * dir;
            camMask.position = lookAt.position + rotation * dir;
        }
        else
        {
            camTransform.transform.position = lookAt.position + rotation * dir;
            camMask.position = lookAt.position + rotation * dir;
        }

        camTransform.transform.LookAt(lookAt);
        Debug.DrawLine(lookAt.position, camTransform.position, Color.black);
    }

    void occludeRay(Vector3 targetFollow)
    {
        RaycastHit wallHit = new RaycastHit();


        if (Physics.Linecast(targetFollow, camMask.position, out wallHit))
        {
            camTransform.position = new Vector3(wallHit.point.x + wallHit.normal.x * 0.5f, camTransform.position.y + wallHit.normal.y * 0.5f, wallHit.point.z + wallHit.normal.z * 0.5f);
            distance = Mathf.Lerp(distance, Mathf.Clamp(wallHit.distance, minDistance, maxDistance), Time.deltaTime * smooth);

        }
        else
        {
            //distance = maxDistance;
            distance = Mathf.Lerp(distance, maxDistance, Time.deltaTime * (smooth /2f));
        }
        Debug.DrawLine(targetFollow, Camera.main.WorldToViewportPoint(new Vector3(Screen.width, Screen.height, 0)), Color.red);

    }
}
