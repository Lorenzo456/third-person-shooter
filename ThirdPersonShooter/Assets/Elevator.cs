﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{

    public Vector3 startDestination;
    public Vector3 endDestination;
    public bool moveOnStart;
    public float speed = 1;
    public float delayBeforeMoving = 2f;
    public bool openOnStart;
    private Vector3 target;
    private Animator anim;


	// Use this for initialization
	void Start ()
	{
	    anim = GetComponent<Animator>();
	    target = transform.position;
        if (moveOnStart)
        {
           Invoke("MoveToDestination", delayBeforeMoving);
        }
	    if (openOnStart)
	    {
	        anim.SetBool("Opened", true);
	    }
	}

    void Update()
    {
        if (transform.position.y != target.y)
        {
          //  Debug.Log("MOVING");
          //  Debug.Log(transform.position);
         //   Debug.Log(target);
            transform.localPosition =  Vector3.MoveTowards(transform.localPosition, target, Time.deltaTime * speed);
            if (transform.position.y == target.y)
            {
                anim.SetBool("Opened", true);
            }
        }
    }

    public void DelayedMove()
    {
        anim.SetBool("Opened", false);
        Invoke("MoveToDestination", delayBeforeMoving);
    }

    public void MoveToDestination()
    {
        //Debug.Log("MOVE LIFT");
        target = endDestination;
        /*
        if (transform.position == startDestination)
        {
            target = endDestination;
        }
        else
        {
            target = startDestination;
        }*/
    }
}
