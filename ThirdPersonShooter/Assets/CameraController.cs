﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    Camera myCamera;
    Transform camTransform;
    public Transform pivot;
    public Transform rightShoulder;
    public Transform character;

    public int charRotationSpeed = 4;
    public int camRotateSpeed = 10;
    public int vertSpeed = 3;
    public bool reverseVertical;

    public float offset = -3;
    float originalOffset;
    public float farthestZoom = -7;
    public float closestZoom = -2;
    float camFollow = 8;
    float camZoom = 1.75f;
    Transform lookAt;
    public float clipLeftOffset;

    LayerMask mask;

    private bool clipping;
    public bool camLookingUp;

    // Use this for initialization
    void Awake()
    {
        //pivot = transform;
        myCamera = Camera.main;
        camTransform = myCamera.transform;
        //character = transform.parent.transform;
        mask = 1 << LayerMask.NameToLayer("Walls");
        lookAt = GameObject.Find("CameraLook").transform;
        camTransform.position = lookAt.TransformPoint(Vector3.forward * offset);
        originalOffset = offset;
    }


    void LateUpdate()
    {

        if(Player.instance.currentHp <= 0 || Time.timeScale == 0)
            return;
        

        //Camera Orbits the charcter Vertically, and Character Rotates Horizontal
        float hor = Input.GetAxis("Mouse X");
        float vert = Input.GetAxis("Mouse Y");

        if (Player.instance.beingGrabbed)
        {
            hor = 0;
            //vert = 0;
        }

        if (!reverseVertical) vert *= -vertSpeed;
        else vert *= vertSpeed;

        hor *= charRotationSpeed;

        //CLAMP Vertical Axis
        float x = pivot.eulerAngles.x;
        if (vert > 0 && x > 61 && x < 270) vert = 0;
        else if (vert < 0 && x < 300 && x > 180) vert = 0;

        pivot.localEulerAngles += new Vector3(vert, 0, 0);
        //Debug.Log(pivot.localEulerAngles.x);
        
        if (pivot.localEulerAngles.x < 355 && pivot.localEulerAngles.x > 298 && !Player.instance.meleeAttack && !Player.instance.beingGrabbed && Player.instance.currentHp > 0)
        {
            Player.instance.aimingUp = true;
            Player.instance.aimingDown = false;
            float max = 298;
            float min = 355;
            float weight = ((pivot.localEulerAngles.x - max) * 100) / (min - max);
            float reverse = 1 * weight;
            float temp = 1 / reverse;
//            Debug.Log(temp * 10);
            Player.instance.animator.SetLayerWeight(2, Mathf.Lerp(Player.instance.animator.GetLayerWeight(2), temp * 20, Time.deltaTime * 50));
        }
        else if(pivot.localEulerAngles.x < 65 && pivot.localEulerAngles.x > 0 && !Player.instance.meleeAttack && !Player.instance.beingGrabbed && Player.instance.currentHp > 0)
        {
            Player.instance.aimingUp = false;
            Player.instance.aimingDown = true;
            float max = 65;
            float min = 0;
            float weight = ((pivot.localEulerAngles.x - max) * 100) / (min - max);
            float reverse = 1 * weight;
            float temp = 1 / reverse;
            //            Debug.Log(temp * 10);
            Player.instance.animator.SetLayerWeight(2, Mathf.Lerp(Player.instance.animator.GetLayerWeight(2), temp * 20, Time.deltaTime * 50));

        }
        else
        {
            Player.instance.aimingUp = false;
            Player.instance.aimingDown = false;
            if (Player.instance.meleeAttack)
            {
                Player.instance.animator.SetLayerWeight(2, 0);
            }
            else
            {
               // Player.instance.animator.SetLayerWeight(2, Mathf.Lerp(Player.instance.animator.GetLayerWeight(2), 0, Time.deltaTime * 20));
            }
        }

        character.Rotate(0, hor, 0);
        rightShoulder.localEulerAngles = pivot.localEulerAngles;
        //clamp zoom
        if (offset > closestZoom) offset = closestZoom;
        if (offset < farthestZoom) offset = farthestZoom;

        //Central Ray
        float unobstructed = offset;
        Vector3 idealPostion = pivot.TransformPoint(Vector3.forward * offset);

        if (Player.instance.aiming)
        {
            offset = Mathf.Lerp(offset, originalOffset * .5f, Time.deltaTime * 10);
        }
        else
        {
            //lookAt.position = pivot.position;
            lookAt.position = Vector3.Lerp(lookAt.position, pivot.position, Time.deltaTime * 30);
            offset = Mathf.Lerp(offset, originalOffset, Time.deltaTime * 10);
        }

        RaycastHit hit;
        if (Physics.Linecast(pivot.position, idealPostion, out hit, mask.value))
        {
            unobstructed = -hit.distance + .01f;
        }

        //smooth
        Vector3 desiredPos = pivot.TransformPoint(Vector3.forward * unobstructed);
        Vector3 currentPos = camTransform.position;


        Vector3 goToPos = new Vector3(Mathf.Lerp(currentPos.x, desiredPos.x, camFollow), Mathf.Lerp(currentPos.y, desiredPos.y, camFollow), Mathf.Lerp(currentPos.z, desiredPos.z, camFollow));

        if (!clipping)
        {
            camTransform.localPosition =  goToPos;

        }
        camTransform.LookAt(lookAt.position);


        //Viewport Bleed prevention
        float c = myCamera.nearClipPlane;
        bool clip = true;
        while (clip)
        {
            Vector3 pos1 = myCamera.ViewportToWorldPoint(new Vector3(0, 0, c));
            Vector3 pos2 = myCamera.ViewportToWorldPoint(new Vector3(.5f, 0, c));
            Vector3 pos3 = myCamera.ViewportToWorldPoint(new Vector3(1, 0, c));
            Vector3 pos4 = myCamera.ViewportToWorldPoint(new Vector3(0, .5f, c));
            Vector3 pos5 = myCamera.ViewportToWorldPoint(new Vector3(1, .5f, c));
            Vector3 pos6 = myCamera.ViewportToWorldPoint(new Vector3(0, 1, c));
            Vector3 pos7 = myCamera.ViewportToWorldPoint(new Vector3(.5f, 1, c));
            Vector3 pos8 = myCamera.ViewportToWorldPoint(new Vector3(1, 1, c));

            Debug.DrawLine(camTransform.position, pos1, Color.yellow);
            Debug.DrawLine(camTransform.position, pos2, Color.yellow);
            Debug.DrawLine(camTransform.position, pos3, Color.yellow);
            Debug.DrawLine(camTransform.position, pos4, Color.yellow);
            Debug.DrawLine(camTransform.position, pos5, Color.yellow);
            Debug.DrawLine(camTransform.position, pos6, Color.yellow);
            Debug.DrawLine(camTransform.position, pos7, Color.yellow);
            Debug.DrawLine(camTransform.position, pos8, Color.yellow);


            if (Physics.Linecast(camTransform.position, pos1, out hit, mask.value))
            {
                // clip
            }
            else if (Physics.Linecast(camTransform.position, pos2, out hit, mask.value))
            {
                // clip
                Debug.Log("CLIP");
            }
            else if (Physics.Linecast(camTransform.position, pos3, out hit, mask.value))
            {
                // clip
         //       Debug.Log("CLIP1");
                camTransform.position = Vector3.Lerp(camTransform.position, new Vector3(hit.point.x, camTransform.position.y, camTransform.position.z) + -camTransform.right * c - new Vector3(0.1f,0,0), camFollow);
                clipping = true;
                clip = false;
            }
            else if (Physics.Linecast(camTransform.position, pos4 - new Vector3(0.2f, 0, 0), out hit, mask.value))
            {
                // clip
           //     Debug.Log("CLIP2");
                camTransform.position = Vector3.Lerp(camTransform.position, new Vector3(hit.point.x, camTransform.position.y, camTransform.position.z) + camTransform.right * c + new Vector3(clipLeftOffset, 0, 0), camFollow);
                clipping = true;
                clip = false;
            }
            else if (Physics.Linecast(camTransform.position, pos5, out hit, mask.value))
            {
                // clip
         //       Debug.Log("CLIP3");
            }
            else if (Physics.Linecast(camTransform.position, pos6, out hit, mask.value))
            {
                // clip
         //       Debug.Log("CLIP4");
            }
            else if (Physics.Linecast(camTransform.position, pos7, out hit, mask.value))
            {
                // clip
          //      Debug.Log("CLIP5");
            }
            else if (Physics.Linecast(camTransform.position, pos8, out hit, mask.value))
            {
                // clip
          //      Debug.Log("CLIP6");
            }
            else clip = false;

          //  Debug.Log("NO CLIPPING");
            if (camTransform.position.z >= 110)
            {
                clip = false;
            }

            if (clip)
            {
                offset = Mathf.Lerp(offset, offset--, Time.deltaTime * 10);
                clipping = false;
            }
            clipping = false;
            return;
        }
    }

}