﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObject : MonoBehaviour, IDamageable
{
    public bool interacted;
    public bool taken;
    public bool onlyIfPlayerIsClose;
    public float distanceNeeded;
    public Transform targetPosition;
    public int hp;

    public void OnDeath()
    {
        
    }

    public void OnHit(int damageTaken = 0)
    {
        hp -= damageTaken;
    }

    public virtual void OnInteraction()
    {
        
    }

    public int ReturnHP()
    {
        return 1;
    }
}
