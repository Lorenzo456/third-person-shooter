﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class footscript : MonoBehaviour {

    void Start()
    {
        AkSoundEngine.SetRTPCValue("Movement", 90);

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "floor")
        {
            // AkSoundEngine.SetSwitch("Footsteps_Floor", other.gameObject.GetComponent<ObjectMaterial>().GetObjectType(), gameObject);
            //AkSoundEngine.PostEvent("Player_Movement", gameObject);
            AkSoundEngine.SetRTPCValue("floor", other.gameObject.GetComponent<ObjectMaterial>().GetObjectType(true));
        }
    }
}
