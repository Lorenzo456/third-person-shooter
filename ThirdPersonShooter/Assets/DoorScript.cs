﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{

    public bool open;

    private Animator anim;

	// Use this for initialization
	void Start ()
	{
	    anim = GetComponent<Animator>();
	    anim.SetBool("open", open);

    }

    public void SwitchDoorState()
    {
        anim.SetTrigger("SwitchState");
    }

    public void DoorSoundOpen(AnimationEvent animationEvent)
    {
        AkSoundEngine.PostEvent("Open_Door", gameObject);
    }

    public void DoorSoundClose(AnimationEvent animationEvent)
    {
        AkSoundEngine.PostEvent("Close_Door", gameObject);
    }
}
