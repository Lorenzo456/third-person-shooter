﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKiller : MonoBehaviour
{

    public bool destroyOnFirstKill;
    private BoxCollider boxCollider;
    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Enemy"))
        {
            Destroy(other.transform.parent.gameObject);
            Gamemanager.instance.currentEnemyCount--;
            if (destroyOnFirstKill)
            {
                boxCollider.enabled = false;
            }
        }
    }
}
