Event	ID	Name			Wwise Object Path	Notes
	1010055213	Enemy_Hit			\Default Work Unit\Enemy\Enemy_Hit	
	1784842228	Enemy_Grab			\Default Work Unit\Enemy\Enemy_Grab	
	1814182224	Enemy_Idle			\Default Work Unit\Enemy\Enemy_Idle	
	2042641261	Enemy_AttackState			\Default Work Unit\Enemy\Enemy_AttackState	
	2311565109	Enemy_Pushed			\Default Work Unit\Enemy\Enemy_Pushed	
	2718137851	Enemy_RunAway			\Default Work Unit\Enemy\Enemy_RunAway	
	3805217580	Enemy_Dead			\Default Work Unit\Enemy\Enemy_Dead	

Switch Group	ID	Name			Wwise Object Path	Notes
	2778287673	Enemy_State			\Default Work Unit\Enemy_State	

Switch	ID	Name	Switch Group			Notes
	1641806523	Attacking	Enemy_State			
	1874288895	Idle	Enemy_State			

State Group	ID	Name			Wwise Object Path	Notes
	892771914	Living			\Default Work Unit\Living	

State	ID	Name	State Group			Notes
	655265632	Alive	Living			
	748895195	None	Living			
	2044049779	Dead	Living			

Custom State	ID	Name	State Group	Owner		Notes
	1061035228	Dead	Living	\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Idle\Idle\Enenmy_Idle_2		

Effect plug-ins	ID	Name	Type				Notes
	212346171	Hall_Guns (Custom)	Wwise RoomVerb			
	819943492	Fear_no_Evil	Wwise Harmonizer			
	1010055213	enemy_hit	Wwise Compressor			
	1674078407	Chorus_Like	Wwise Flanger			
	2080872166	Brick_Wall_Minus_6dB_Peak_Fast_Release	Wwise Peak Limiter			

Source plug-ins	ID	Name	Type		Wwise Object Path	Notes
	214439335	Wwise Silence	Wwise Silence		\Actor-Mixer Hierarchy\Default Work Unit\Player\Dialoog\Dialoog\Edited\Enemy_Hit\Enemy_Hit\Silence\Wwise Silence	
	250936181	Wwise Silence	Wwise Silence		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Idle\Idle\Silence\Wwise Silence	
	269860911	Wwise Silence	Wwise Silence		\Actor-Mixer Hierarchy\Default Work Unit\Player\Dialoog\Dialoog\Edited\Enemy_Hit\Enemy_Hit\Silence_03\Wwise Silence	
	886260272	Wwise Silence	Wwise Silence		\Actor-Mixer Hierarchy\Default Work Unit\Player\Dialoog\Dialoog\Edited\Enemy_Hit\Enemy_Hit\Silence_02\Wwise Silence	
	957924523	Wwise Silence	Wwise Silence		\Actor-Mixer Hierarchy\Default Work Unit\Player\Dialoog\Dialoog\Edited\Enemy_Hit\Enemy_Hit\Silence_01\Wwise Silence	
	1037734193	Wwise Silence	Wwise Silence		\Actor-Mixer Hierarchy\Default Work Unit\Player\Dialoog\Dialoog\Edited\Enemy_Hit\Silence\Wwise Silence	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	149267797	Enemy_Hit_11	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit11_FDC40602.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_11		154640
	154907499	Enemy_Hit_10	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit10_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_10		112616
	190361427	Enenmy_Idle_2	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enenmy_Idle_2_0F232879.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Idle\Idle\Enenmy_Idle_2		2558356
	222476151	Attacking_3	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Attacking_3_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Attacking\Enemy_State\Attack_State\Attacking_3		651748
	223107896	Enemy_Hit_9	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit9_04A0C16F.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_9		118236
	227634901	Enemy_Hit_7	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit7_67E1FEFC.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_7		68108
	233322415	Enemy_Hit_4	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit4_1B03A059.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_4		109228
	326366836	Attacking_5	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Attacking_5_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Attacking\Enemy_State\Attack_State\Attacking_5		383760
	328805767	Attacking_2	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Attacking_2_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Attacking\Enemy_State\Attack_State\Attacking_2		450900
	341499902	Dying5	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Dying5_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Dying\Dying\Dying_Layer1\Dying5		108712
	345365972	Enemy_Hit_1	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit_1_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_1		98048
	384475105	Edited_Fuck you buddy_2	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\Voices\English(US)\Edited\Edited_Fuck you buddy_2_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Player\Dialoog\Dialoog\Edited\Enemy_Hit\Enemy_Hit\Text\Edited_Fuck you buddy_2		276560
	392351887	Attacking_4	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Attacking_4_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Attacking\Enemy_State\Attack_State\Attacking_4		457048
	392444125	Dying3	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Dying3_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Dying\Dying\Dying_Layer1\Dying3		210272
	426743329	Dying2	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Dying2_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Dying\Dying\Dying_Layer1\Dying2		217320
	557638645	DyingLayer2_1	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\DyingLayer2_1_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Dying\Dying\Dying_Layer2\DyingLayer2_1		111904
	560740083	Attacking_1	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Attacking_1_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Attacking\Enemy_State\Attack_State\Attacking_1		419856
	621136100	Edited_You like that you little fuckers you like that_2	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\Voices\English(US)\Edited_You like that you little fuckers you like that_2_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Player\Dialoog\Dialoog\Edited\Enemy_Hit\Enemy_Hit\Text\You like that you fuckers\Edited_You like that you little fuckers you like that_2		374500
	633898443	Edited_You like that you little fuckers you like that_3	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\Voices\English(US)\Edited\Edited_You like that you little fuckers you like that_3_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Player\Dialoog\Dialoog\Edited\Enemy_Hit\Enemy_Hit\Text\You like that you fuckers\Edited_You like that you little fuckers you like that_3		347812
	654947421	Edited_I did not hit here i did not	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\Voices\English(US)\Edited\Edited_I did not hit here i did not_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Player\Dialoog\Dialoog\Edited\Enemy_Hit\Enemy_Hit\Text\Edited_I did not hit here i did not		438640
	764214674	Enemy_Hit_5	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit5_6768AFD2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_5		103452
	900025304	Dying4	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Dying4_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Dying\Dying\Dying_Layer1\Dying4		144900
	942299880	Enemy_Hit_12	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit12_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_12		126800
	943392276	Enemy_Hit_8	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit8_7A879CEA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_8		84944
	975662765	TrowOutOfGrab	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\TrowOutOfGrab_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Player\TrowOutOfGrab		573328
	977914701	Enemy_Hold_Player_1	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hold_Player_1_7F118B31.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Attacking\Enemy_Hold_Player_1		1302884
	1020461917	Enenmy_Idle	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enenmy_Idle_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Idle\Idle\Enenmy_Idle		996096
	1020874465	Dying_layer2_2	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Dying_layer2_2_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Dying\Dying\Dying_Layer2\Dying_layer2_2		126272
	1031959430	Dying_layer2_3	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Dying_layer2_3_4D93CF03.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Dying\Dying\Dying_Layer2\Dying_layer2_3		81444
	1043920661	Enemy_Hit_6	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit6_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_6		94316
	1046218287	DyingLayer3_1	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\DyingLayer3_1_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Dying\Dying\Dying_Layer3\DyingLayer3_1		126272
	1052763773	Enemy_Hit_2	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\SFX\Enemy_Hit2_C663BAD2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Enemy\Hit\Enemy_Hit_2		82052
	1072081122	Edited_You like that you little fuckers you like that	C:\Users\Asus\Documents\WwiseProjects\Third person shooter\.cache\Windows\Voices\English(US)\Edited\Edited_You like that you little fuckers you like that_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Player\Dialoog\Dialoog\Edited\Enemy_Hit\Enemy_Hit\Text\You like that you fuckers\Edited_You like that you little fuckers you like that		431704

