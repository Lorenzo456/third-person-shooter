Switch Group	ID	Name			Wwise Object Path	Notes
	550365015	Footsteps_Enemy			\Default Work Unit\Footsteps_Enemy	
	1510754372	Footsteps_Speed_01			\Default Work Unit\Player\Footsteps_Speed_01	
	3182351798	Footsteps_Speed			\Default Work Unit\Player\Footsteps_Speed	

Switch	ID	Name	Switch Group			Notes
	340271938	Walking	Footsteps_Enemy			
	2702706662	Standing_still	Footsteps_Enemy			
	3863236874	Running	Footsteps_Enemy			
	2666120782	E_Fast_Sprinting	Footsteps_Speed_01			
	3539943603	C_VerySlow_Sneaking	Footsteps_Speed_01			
	3670211787	D_Medium_Running	Footsteps_Speed_01			
	3907997667	B_Slow_walking	Footsteps_Speed_01			
	2666120782	E_Fast_Sprinting	Footsteps_Speed			
	3539943603	C_VerySlow_Sneaking	Footsteps_Speed			
	3670211787	D_Medium_Running	Footsteps_Speed			
	3907997667	B_Slow_walking	Footsteps_Speed			

State Group	ID	Name			Wwise Object Path	Notes
	892771914	Living			\Default Work Unit\Living	
	1725070628	Muziek			\Default Work Unit\Muziek	
	2077253480	Room			\Default Work Unit\Room	
	2764240573	Combat			\Default Work Unit\Combat	

State	ID	Name	State Group			Notes
	655265632	Alive	Living			
	748895195	None	Living			
	2044049779	Dead	Living			
	349818688	Begin	Muziek			
	579523862	Explore	Muziek			
	748895195	None	Muziek			
	856861708	Grabbed	Muziek			
	1265788330	Battle_Kort	Muziek			
	1725070628	Muziek	Muziek			
	3157003241	Test	Muziek			
	748895195	None	Room			
	1359360137	Room1	Room			
	1359360138	Room2	Room			
	1359360139	Room3	Room			
	1359360140	Room4	Room			
	1359360141	Room5	Room			
	1359360142	Room6	Room			
	1359360143	Room7	Room			
	1595813765	Hallway_1	Room			
	3438749302	Ending_titles	Room			
	748895195	None	Combat			
	3373579172	InCombat	Combat			

Custom State	ID	Name	State Group	Owner		Notes
	100721004	Dead	Living	\Master-Mixer Hierarchy\Default Work Unit\Master Audio Bus\Enemy\Enemy screams		

Game Parameter	ID	Name			Wwise Object Path	Notes
	477830752	Finisher_SideChain			\Default Work Unit\Finisher_SideChain	
	2202570721	Dialoog_Volume			\Default Work Unit\Dialoog_Volume	
	3641119360	Enemy_Screams			\Default Work Unit\Enemy_Screams	

Audio Bus	ID	Name			Wwise Object Path	Notes
	1244013424	Critical_Dialoog			\Default Work Unit\Master Audio Bus\Dialoog\Critical_Dialoog	
	1679256492	Non_Enemy_Screams			\Default Work Unit\Master Audio Bus\Enemy\Non_Enemy_Screams	
	2129177713	Enemy screams			\Default Work Unit\Master Audio Bus\Enemy\Enemy screams	
	2299321487	Enemy			\Default Work Unit\Master Audio Bus\Enemy	
	3683254069	Finisher			\Default Work Unit\Master Audio Bus\Finisher	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	
	3945355853	Non_Critical_Sounds			\Default Work Unit\Master Audio Bus\Dialoog\Non_Critical_Sounds	
	4102640372	Dialoog			\Default Work Unit\Master Audio Bus\Dialoog	

Auxiliary Bus	ID	Name			Wwise Object Path	Notes
	1731472929	Reverb_Medium_Room			\Default Work Unit\Master Audio Bus\Reverb_Medium_Room	
	1852587007	Big_Garage			\Default Work Unit\Master Audio Bus\Big_Garage	
	1883033791	Sidechain			\Default Work Unit\Master Audio Bus\Sidechain	
	4063189299	Corridor			\Default Work Unit\Master Audio Bus\Corridor	

Effect plug-ins	ID	Name	Type				Notes
	377925139	Wwise Meter (Custom)	Wwise Meter			
	458129169	Wwise Compressor (Custom)	Wwise Compressor			
	670950587	Wwise Meter (Custom)	Wwise Meter			
	920747709	Wwise Meter (Custom)	Wwise Meter			
	1904434771	Medium_Room1	Wwise Matrix Reverb			
	2995775460	Room_Small	Wwise RoomVerb			
	3546444380	Big_Garage1	Wwise Matrix Reverb			

Audio Devices	ID	Name	Type				Notes
	530303819	Communication	Communication			
	2317455096	No_Output	No Output			
	3859886410	System	System			

