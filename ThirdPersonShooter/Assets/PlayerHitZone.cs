﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitZone : MonoBehaviour
{
    public BoxCollider collider;

    void Start()
    {
        collider = GetComponent<BoxCollider>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
      //      other.GetComponent<Enemy>().inPlayerHitZone = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
       //    other.GetComponent<Enemy>().inPlayerHitZone = false;
        }
    }
}
