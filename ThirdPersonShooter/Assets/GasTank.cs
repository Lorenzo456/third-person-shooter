﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasTank : InteractiveObject
{
    private ParticleSystem particleSystem;
	// Use this for initialization
	void Start ()
	{
	    particleSystem = GetComponent<ParticleSystem>();

	}

    public override void OnInteraction()
    {
        interacted = true;
        particleSystem.Play();
    }
}
