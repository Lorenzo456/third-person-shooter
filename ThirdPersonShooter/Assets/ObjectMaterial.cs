﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMaterial : MonoBehaviour {

    public enum MaterialType { Concrete, Stairs};
    public MaterialType materialType;
     

    public String GetObjectType()
    {
        if (materialType == MaterialType.Concrete)
        {
            return "Concrete";
        }

        if (materialType == MaterialType.Stairs)
        {
            return "Stair";
        }
        return "";
    }

    public float GetObjectType(bool useFloats)
    {
        if (materialType == MaterialType.Concrete)
        {
            return 40;
        }

        if (materialType == MaterialType.Stairs)
        {
            return 90;
        }
        return 40;
    }
	// Update is called once per frame
	void Update () {
		
	}
}
