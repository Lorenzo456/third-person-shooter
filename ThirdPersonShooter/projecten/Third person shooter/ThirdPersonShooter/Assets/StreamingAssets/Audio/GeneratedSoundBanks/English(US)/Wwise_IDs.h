/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AFSTAND_LEGE_DEUR = 2405581441U;
        static const AkUniqueID ALIVE = 655265632U;
        static const AkUniqueID CHEMOTANK = 2789890545U;
        static const AkUniqueID CLOSE_DOOR = 1307892918U;
        static const AkUniqueID COMPUTER_ON = 3462245978U;
        static const AkUniqueID DEAD = 2044049779U;
        static const AkUniqueID DESTROYED_PC = 992827562U;
        static const AkUniqueID ELEVATOR = 3705378287U;
        static const AkUniqueID ENEMY_ATTACKSTATE = 2042641261U;
        static const AkUniqueID ENEMY_DEAD = 3805217580U;
        static const AkUniqueID ENEMY_GRAB = 1784842228U;
        static const AkUniqueID ENEMY_HIT = 1010055213U;
        static const AkUniqueID ENEMY_IDLE = 1814182224U;
        static const AkUniqueID ENEMY_PUSHED = 2311565109U;
        static const AkUniqueID ENEMY_RUNAWAY = 2718137851U;
        static const AkUniqueID GUN_EMPTY = 3619263103U;
        static const AkUniqueID LEGE_DEUR_OPEN = 2788713406U;
        static const AkUniqueID LIGHT_BULP = 1191201081U;
        static const AkUniqueID LIGHT_BULP_BROKEN = 1836793139U;
        static const AkUniqueID LIGHT_BULP_FLICKER = 2617528080U;
        static const AkUniqueID MUZIEK = 1725070628U;
        static const AkUniqueID PLAYER_HIT = 871813740U;
        static const AkUniqueID PLAYER_MOVEMENT = 541470702U;
        static const AkUniqueID RELOAD = 456382354U;
        static const AkUniqueID ROOM_MUSIC = 1808177020U;
        static const AkUniqueID ROOMTONE = 2291679880U;
        static const AkUniqueID SET_TOROOM3 = 1901828055U;
        static const AkUniqueID SHOTGUN = 51683977U;
        static const AkUniqueID STOP_MUZIEK = 3879642213U;
        static const AkUniqueID STOP_PLAYER_MOVEMENT = 1386092461U;
        static const AkUniqueID TANK = 3206747537U;
        static const AkUniqueID WOODEN_DOOR_BROKEN = 2119922866U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace COMBAT
        {
            static const AkUniqueID GROUP = 2764240573U;

            namespace STATE
            {
                static const AkUniqueID INCOMBAT = 3373579172U;
            } // namespace STATE
        } // namespace COMBAT

        namespace LIVING
        {
            static const AkUniqueID GROUP = 892771914U;

            namespace STATE
            {
                static const AkUniqueID ALIVE = 655265632U;
                static const AkUniqueID DEAD = 2044049779U;
            } // namespace STATE
        } // namespace LIVING

        namespace MUZIEK
        {
            static const AkUniqueID GROUP = 1725070628U;

            namespace STATE
            {
                static const AkUniqueID BATTLE_KORT = 1265788330U;
                static const AkUniqueID BEGIN = 349818688U;
                static const AkUniqueID EXPLORE = 579523862U;
                static const AkUniqueID GRABBED = 856861708U;
                static const AkUniqueID MUZIEK = 1725070628U;
                static const AkUniqueID TEST = 3157003241U;
            } // namespace STATE
        } // namespace MUZIEK

        namespace ROOM
        {
            static const AkUniqueID GROUP = 2077253480U;

            namespace STATE
            {
                static const AkUniqueID ELEVATOR = 3705378287U;
                static const AkUniqueID ENDING_TITLES = 3438749302U;
                static const AkUniqueID HALLWAY_1 = 1595813765U;
                static const AkUniqueID ROOM1 = 1359360137U;
                static const AkUniqueID ROOM2 = 1359360138U;
                static const AkUniqueID ROOM3 = 1359360139U;
                static const AkUniqueID ROOM4 = 1359360140U;
                static const AkUniqueID ROOM5 = 1359360141U;
                static const AkUniqueID ROOM6 = 1359360142U;
                static const AkUniqueID ROOM7 = 1359360143U;
            } // namespace STATE
        } // namespace ROOM

    } // namespace STATES

    namespace SWITCHES
    {
        namespace ENEMY_STATE
        {
            static const AkUniqueID GROUP = 2778287673U;

            namespace SWITCH
            {
                static const AkUniqueID ATTACKING = 1641806523U;
                static const AkUniqueID IDLE = 1874288895U;
            } // namespace SWITCH
        } // namespace ENEMY_STATE

        namespace FOOTSTEP_FLOOR
        {
            static const AkUniqueID GROUP = 997064848U;

            namespace SWITCH
            {
                static const AkUniqueID CONCRETE = 841620460U;
                static const AkUniqueID STAIR = 1164367372U;
            } // namespace SWITCH
        } // namespace FOOTSTEP_FLOOR

        namespace FOOTSTEPS_ENEMY
        {
            static const AkUniqueID GROUP = 550365015U;

            namespace SWITCH
            {
                static const AkUniqueID RUNNING = 3863236874U;
                static const AkUniqueID STANDING_STILL = 2702706662U;
                static const AkUniqueID WALKING = 340271938U;
            } // namespace SWITCH
        } // namespace FOOTSTEPS_ENEMY

        namespace FOOTSTEPS_SPEED
        {
            static const AkUniqueID GROUP = 3182351798U;

            namespace SWITCH
            {
                static const AkUniqueID B_SLOW_WALKING = 3907997667U;
                static const AkUniqueID C_VERYSLOW_SNEAKING = 3539943603U;
                static const AkUniqueID D_MEDIUM_RUNNING = 3670211787U;
                static const AkUniqueID E_FAST_SPRINTING = 2666120782U;
            } // namespace SWITCH
        } // namespace FOOTSTEPS_SPEED

        namespace FOOTSTEPS_SPEED_01
        {
            static const AkUniqueID GROUP = 1510754372U;

            namespace SWITCH
            {
                static const AkUniqueID B_SLOW_WALKING = 3907997667U;
                static const AkUniqueID C_VERYSLOW_SNEAKING = 3539943603U;
                static const AkUniqueID D_MEDIUM_RUNNING = 3670211787U;
                static const AkUniqueID E_FAST_SPRINTING = 2666120782U;
            } // namespace SWITCH
        } // namespace FOOTSTEPS_SPEED_01

        namespace PLAYER_HEALTH
        {
            static const AkUniqueID GROUP = 215992295U;

            namespace SWITCH
            {
                static const AkUniqueID FULL = 2510516222U;
                static const AkUniqueID HALF = 3633416822U;
                static const AkUniqueID ZERO = 766521211U;
            } // namespace SWITCH
        } // namespace PLAYER_HEALTH

        namespace SITUATION
        {
            static const AkUniqueID GROUP = 4223905515U;

            namespace SWITCH
            {
                static const AkUniqueID COMBAT = 2764240573U;
                static const AkUniqueID EXPLORE = 579523862U;
            } // namespace SWITCH
        } // namespace SITUATION

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID DIALOOG_VOLUME = 2202570721U;
        static const AkUniqueID ENEMY_MOVEMENT = 62399725U;
        static const AkUniqueID ENEMY_SCREAMS = 3641119360U;
        static const AkUniqueID FIGHTING = 168610243U;
        static const AkUniqueID FINISHER_SIDECHAIN = 477830752U;
        static const AkUniqueID HEALTH = 3677180323U;
        static const AkUniqueID MOVEMENT = 2129636626U;
        static const AkUniqueID PIANO_STINGER = 2286146679U;
        static const AkUniqueID ROOM = 2077253480U;
        static const AkUniqueID ROOMTONE = 2291679880U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
        static const AkUniqueID UNDERATTACK = 2862835585U;
    } // namespace GAME_PARAMETERS

    namespace TRIGGERS
    {
        static const AkUniqueID AMBIENCE_STINGER_MUSICAL = 2352308577U;
        static const AkUniqueID AMBIENCE_STINGERS = 501575837U;
        static const AkUniqueID ENEMY_KILLED = 480881753U;
        static const AkUniqueID STINGER_1 = 1999150977U;
    } // namespace TRIGGERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID DIALOOG = 4102640372U;
        static const AkUniqueID ENEMY = 2299321487U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID OBJECTS = 1695690031U;
        static const AkUniqueID PLAYER = 1069431850U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID CRITICAL_DIALOOG = 1244013424U;
        static const AkUniqueID DIALOOG = 4102640372U;
        static const AkUniqueID ENEMY = 2299321487U;
        static const AkUniqueID ENEMY_SCREAMS = 2129177713U;
        static const AkUniqueID FINISHER = 3683254069U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID NON_CRITICAL_SOUNDS = 3945355853U;
        static const AkUniqueID NON_ENEMY_SCREAMS = 1679256492U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID BIG_GARAGE = 1852587007U;
        static const AkUniqueID CORRIDOR = 4063189299U;
        static const AkUniqueID REVERB_MEDIUM_ROOM = 1731472929U;
        static const AkUniqueID SIDECHAIN = 1883033791U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID COMMUNICATION = 530303819U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
